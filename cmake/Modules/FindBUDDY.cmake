# - Try to find ImageMagick++
# Once done, this will define
#
#  BUDDY_FOUND - system has BUDDY
#  BUDDY_INCLUDE_DIRS - the BUDDY include directories
#  BUDDY_LIBRARIES - link these to use BUDDY

include(LibFindMacros)

# Dependencies
#libfind_package(BUDDY Magick)

# Use pkg-config to get hints about paths
#libfind_pkg_check_modules(BUDDY_PKGCONF ImageBUDDY)

# Include dir
find_path(BUDDY_INCLUDE_DIR
  NAMES bdd.h
)

# Finally the library itself
find_library(BUDDY_LIBRARY
  NAMES bdd
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(BUDDY_PROCESS_INCLUDES BUDDY_INCLUDE_DIR)
set(BUDDY_PROCESS_LIBS BUDDY_LIBRARY)
libfind_process(BUDDY)

