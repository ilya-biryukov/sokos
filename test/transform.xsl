<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
     This is a transformation to convert .slc files into text 
  -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" indent="no"/>
<xsl:template match="/">
  <xsl:for-each select="SokobanLevels/LevelCollection/Level">
    <xsl:for-each select="L">
      <xsl:value-of select="concat(., '&#10;')" />
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
