{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}

import Shelly
import qualified Data.Text as T
import Prelude hiding (FilePath)

import Data.Monoid
import System.Environment
import System.Posix
import Data.List(sortBy)
import Control.Monad.STM
import Control.Concurrent.STM.TMVar
import Control.Monad
import Control.DeepSeq
import Control.DeepSeq.TH
import Control.Concurrent

default (T.Text)

type ErrCode = Int

data InputTask 
  = InputTask { 
      itPath :: FilePath,
      itTimeout :: T.Text
    }

$(deriveNFData ''InputTask)

data Output 
  = Success T.Text 
  | Failure ErrCode

data RunnerResult = RunnerResult {
    rrSucceeded :: [InputTask],
    rrFailed :: [InputTask],
    rrTimedOut :: [InputTask]
  } 

$(deriveNFData ''RunnerResult)
  

instance Monoid (RunnerResult) where
  mempty = RunnerResult [] [] []
  (RunnerResult lx ly lz) `mappend` (RunnerResult rx ry rz) 
    = RunnerResult (lx <> rx) (ly <> ry) (lz <> rz)

timeout :: T.Text -> FilePath -> [T.Text] -> Sh T.Text
timeout tout cmd_ args = run "timeout" (tout:toTextIgnore cmd_:args)

timeoutSokos :: T.Text -> FilePath -> Sh T.Text
timeoutSokos tout file = timeout tout "./sokos" [toTextIgnore file]

outputDir :: FilePath
outputDir = "output/"

outPathTxt :: FilePath -> FilePath
outPathTxt path_ = outputDir <> path_

outPath :: InputTask -> FilePath
outPath = outPathTxt . itPath

runSokosOutput :: T.Text -> FilePath -> Sh Output
runSokosOutput tout path_ = silently $ do 
  out <- timeoutSokos tout path_
  ec <- lastExitCode
  case ec of
    0 -> return $ Success out
    n -> return $ Failure n
  
runTask :: InputTask -> Sh RunnerResult
runTask t@(InputTask path_ tout) = do
  echo $ "Running for " <> toTextIgnore path_
  errExit False $ do
    out <- runSokosOutput tout path_
    case out of 
      (Success out_) -> printOutput out_ >> return (RunnerResult [t] [] [])
      (Failure 124) -> return $ RunnerResult [] [] [t]
      (Failure _)   -> return $ RunnerResult [] [t] []
    where
      printOutput = writefile (outPath t)

getFileSize :: String -> IO FileOffset
getFileSize fpath = fileSize <$> getFileStatus fpath

runTasks :: [InputTask] -> Sh RunnerResult
runTasks tasks = mconcat <$> mapM runTask tasks

dumpTaskPathsToFile :: FilePath -> [InputTask] -> Sh ()
dumpTaskPathsToFile targetFilePath 
  = writefile targetFilePath . T.unlines . map (\(InputTask tsk _) -> toTextIgnore tsk)

printStats :: RunnerResult -> IO ()
printStats (RunnerResult succs fails touts) = do
  putStrLn ("Total: " ++ show (length succs + length fails + length touts))
  putStrLn ("Succeeded:" ++ show (length succs))
  putStrLn ("Timeouts:" ++ show (length touts))
  putStrLn ("Faliures:" ++ show (length fails))
  unless (null touts) $ putStrLn "List of timeouts:" >> printTasks touts
  unless (null fails) $ putStrLn "List of failures:" >> printTasks fails
  where    
    printTasks = mapM_ (
      \(InputTask tsk _) -> (putStrLn $ '\t' : show (toTextIgnore tsk)))    

threadCount :: Int
threadCount = 4

splitTo :: Integral a => [b] -> a -> [[b]]
splitTo lst n 
  = map (\n -> map snd . filter (predN n) $ zip [1..] lst) [0..n-1]
  where
    predN k (i, _) =  (i `mod` n) == k

main :: IO ()
main = do
  files <- getArgs
  files <- sortBySize files
  let paths = map (fromText . T.pack) files
  let touts = ["3s", "30s", "1m"] 
  loopTasks touts paths
  where 
    loopTasks [] _ = return ()
    loopTasks (t:ts) paths = do
      putStrLn ("Running with timeout " <> T.unpack t)
      results <- replicateM threadCount newEmptyTMVarIO 
      let splitedTasks = tasks `splitTo` threadCount
      mapM_ (\ (var, tasks) -> forkIO $ do                 
                res <- shelly $ runTasks tasks >>= flip dumpIfFailedAndReturn t
                let resEval = res `deepseq` res
                atomically $ putTMVar var resEval
                ) $ zip results splitedTasks

      separate_results <- mapM (atomically . readTMVar) results
      let res = mconcat separate_results
      printStats res
      unless (null (rrTimedOut res)) $
        loopTasks ts (map itPath $ rrTimedOut res)
      where
        tasks = map (`InputTask` t) paths
    sortBySize files = do
      fileWithSizes <- mapM (\f -> (f,) <$> getFileSize f) files
      return $ map fst $ sortBy (\(_,x) (_,y) -> compare x y) fileWithSizes
    dumpIfFailedAndReturn rr tout = do
      unless (null failed) (dumpTaskPathsToFile 
        (outPathTxt (fromText $ "__failed." <> tout)) failed)
      unless (null timeouts) (dumpTaskPathsToFile 
        (outPathTxt (fromText $ "__timeouts." <> tout)) timeouts)
      return rr
      where
        failed = rrFailed rr
        timeouts = rrTimedOut rr
