module Sokoban.Data.Game where

import qualified Data.Set as DS

data InputCell = IcFloor | IcWall | IcPerson | IcBox | IcTarget | IcBoxTarget | IcPersonTarget
  deriving Eq

readInputCell :: Char -> InputCell
readInputCell ' ' = IcFloor
readInputCell '#' = IcWall
readInputCell '@' = IcPerson
readInputCell '$' = IcBox
readInputCell '.' = IcTarget
readInputCell '*' = IcBoxTarget
readInputCell '+' = IcPersonTarget

type Coords = (Int, Int)

data Position = Position {
    psPlayer :: Coords,
    psBoxes :: [Coords]
 }

data LevelDescr = LeveDescr {
    ldWalls :: DS.Set Coords,
    ldTargets :: DS.Set Coords,
    ldInitialPosition :: Position
 }

data Direction = DirUp | DirDown | DirLeft | DirRight
  deriving Enum

allDirections :: [Direction]
allDirections = [DirUp ..]

cellAtDirection :: Direction -> Coords -> Coords
cellAtDirection DirUp    (x, y) = (x, y - 1)
cellAtDirection DirDown  (x, y) = (x, y + 1)
cellAtDirection DirLeft  (x, y) = (x - 1, y)
cellAtDirection DirRight (x, y) = (x + 1, y)
