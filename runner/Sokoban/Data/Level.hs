module Sokoban.Data.Level(
  readLevel,
  levelAreaAndPerimiter
) where

import Sokoban.Data.Game
import Sokoban.Algorithms.Search
import qualified Data.Set as DS
import Control.Lens.Tuple
import Control.Lens.Setter
import Control.Arrow((>>>))

readLevel :: [String] -> LevelDescr
readLevel = map (map readInputCell) >>> zip [0..] >>> foldl appendRow initState
    >>> (\(ws, tgts, bxs, Just pos) -> LeveDescr ws tgts $ Position pos bxs)
  where
    replaceNothing x Nothing  = Just x
    replaceNothing _ (Just _) = error "value was already set"

    initState = (DS.empty, DS.empty, [], Nothing)

    appendRow s (r, cells) = foldl (appendInputCell r) s (zip [0..] cells)

    appendInputCell row s (col, cell) = updateState cell (col, row) s

    updateState IcWall         c = addWall c
    updateState IcPerson       c = setPlayerPos c
    updateState IcPersonTarget c = setPlayerPos c . addTarget c
    updateState IcBox          c = addBox c
    updateState IcBoxTarget    c = addBox c . addTarget c
    updateState IcTarget       c = addTarget c
    updateState IcFloor        _ = id

    addWall c      = over _1 (DS.insert c)
    addTarget c    = over _2 (DS.insert c)
    addBox c       = over _3 (c:)
    setPlayerPos c = over _4 (replaceNothing c)


levelAreaAndPerimiter :: LevelDescr -> (Int, Int)
levelAreaAndPerimiter lvl = dfs [psPlayer $ ldInitialPosition lvl] getMoves visitor (0, 0)
  where
    visitor = mkDfsVisitor addAreaAndPerimiter
    addAreaAndPerimiter pos (a, p) = (a+1, p + length (getWalls pos))
    getWalls pos = (map (`cellAtDirection` pos) >>> filter isWall) allDirections
    getMoves pos = (map (`cellAtDirection` pos) >>> filter canPlaceAt) allDirections
    walls = ldWalls lvl    
    canPlaceAt = (`DS.notMember` walls)
    isWall = (`DS.member` walls)
