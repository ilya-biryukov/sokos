module Sokoban.Algorithms.Search(
  dfs,
  DfsVisitor(),
  mkEmptyDfsVisitor,
  mkDfsVisitor
) where

import qualified Data.Set as DS
import Data.Sequence((><), ViewL ((:<)))
import qualified Data.Sequence as Seq

data DfsVisitor a res = DfsVisitor {
    dvOpenVertex :: a -> res -> res
  }

mkEmptyDfsVisitor :: DfsVisitor (res -> res) res
mkEmptyDfsVisitor = DfsVisitor id

mkDfsVisitor :: (a -> res -> res) -> DfsVisitor a res
mkDfsVisitor = DfsVisitor

dfs :: (Ord a) => [a] -> (a -> [a]) -> DfsVisitor a res -> res -> res
dfs startStates transition visitor = go (Seq.fromList startStates) DS.empty
  where
    openVertex = dvOpenVertex visitor
    go queued visited val
      | Seq.null queued = val
      | otherwise       
        = let (x :< xs) = Seq.viewl queued 
              neighbours = filter (`DS.notMember` visited) (transition x)
          in 
            if x `DS.notMember` visited 
              then go (xs >< Seq.fromList neighbours) 
                      (x `DS.insert` visited)
                      (openVertex x val) 
              else go xs visited val
