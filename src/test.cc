#include "sokos_defs.h"

#include <fstream>
#include <array>
#include <iostream>
#include <boost/dynamic_bitset.hpp>

#include "sokoban/sokoban.h"
#include "sokoban/search/dfs.hpp"
#include "sokoban/search/astar.hpp"
#include "sokoban/search/transitions.hpp"
#include "sokoban/search/field_descriptor.hpp"
#include "sokoban/search/restore_path.hpp"
#include "sokoban/search/printers.hpp"
#include "sokoban/search/packing_order.hpp"
#include "deadlocks/simple.hpp"
#include "deadlocks/freeze.hpp"

using namespace sokoban;
using namespace sokoban::search;
using namespace sokoban::deadlocks;


sokoban::cell_state state_by_char(char c) {
  using namespace sokoban;

  if (c == ' ')
    return cell_state::Empty;
  if (c == '#')
    return cell_state::Wall;
  if (c == '@')
    return cell_state::Player;
  if (c == '$')
    return cell_state::Box;
  if (c == '.')
    return cell_state::Target;
  if (c == '*')
    return cell_state::BoxTarget;
  if (c == '+')
    return cell_state::PlayerTarget;

  assert(false && "unknown character");
  return cell_state::Empty;
}

char char_by_state(sokoban::cell_state c) {
  using namespace sokoban;

  if (c == cell_state::Empty)
    return ' ';
  if (c == cell_state::Wall)
    return '#';
  if (c == cell_state::Player)
    return '@';
  if (c == cell_state::Box)
    return '$';
  if (c == cell_state::Target)
    return '.';
  if (c == cell_state::BoxTarget)
    return '*';
  if (c == cell_state::PlayerTarget)
    return '+';

  assert(false && "unknown cell_state");
  return '?';
}

classic_search::field_descriptor descriptor_from_field(sokoban::field const & f) {
  using namespace sokoban;
  using namespace classic_search;

  std::vector<point> walls;
  std::vector<point> goals;
  point player;
  for (std::size_t x = 0; x < f.width(); ++x) {
    for (std::size_t y = 0; y < f.height(); ++y) {
      if (f(x, y) == cell_state::Wall) {
        walls.emplace_back((int)x, (int)y);
      } else if (f(x, y) == cell_state::Target
              || f(x, y) == cell_state::BoxTarget
              || f(x, y) == cell_state::PlayerTarget) {
        goals.emplace_back((int)x, (int)y);
      }

      if (f(x, y) == cell_state::Player || f(x, y) == cell_state::PlayerTarget) {
        player = {x, y};
      }
    }
  }

  return field_descriptor(f.width(), f.height(), walls, goals, player);
}


template<class BoxesOutIt>
void find_boxes_and_player(sokoban::field const & f, BoxesOutIt out, point & player) {
  for (std::size_t x = 0; x < f.width(); ++x) {
    for (std::size_t y = 0; y < f.height(); ++y) {
      if (f(x, y) == cell_state::Box || f(x, y) == cell_state::BoxTarget) {
        *out++ = point(x, y);
      }
      if (f(x,y) == cell_state::Player || f(x,y) == cell_state::PlayerTarget) {
        player = {x, y};
      }
    }
  }
}

sokoban::search::box_state initial_pos(field_descriptor const & fd, sokoban::field const & f) {
  std::vector<point> boxes;
  point player;
  find_boxes_and_player(f, std::back_inserter(boxes), player);

  return make_box_state(fd, boxes, player);
}

sokoban::field reverse_search_field(sokoban::field const & f ) {
  sokoban::field reverse_field(f);

  auto to_reverse = [](cell_state c) {
    switch (c) {
      case cell_state::Wall:
      case cell_state::Empty:
      case cell_state::BoxTarget:
        return c;
      case cell_state::Target:
      case cell_state::PlayerTarget:
        return cell_state::Box;
      case cell_state::Box:
        return cell_state::Target;
      case cell_state::Player:
        return cell_state::Empty;
      default:
        assert(false && "invlaid cell_state");
        return cell_state::Empty;
    }
  };

  for (int x = 0; x < reverse_field.width(); ++x) {
    for (int y = 0; y < reverse_field.height(); ++y) {
      reverse_field(x, y) = to_reverse(reverse_field(x, y));
    }
  }

  return reverse_field;
}

struct transition {
public:
  using mark_data = backtrack_data;

private:
  field_descriptor const & fd_;
  simple_deadlock_set const & dlocks_;

public:
  transition(field_descriptor const & fd, simple_deadlock_set const & dlocks)
      : fd_(fd)
      , dlocks_(dlocks) {
  }

  template<class OutIt>
  void operator()(box_state const & s, OutIt out) {
      return simple_push_transitions(
          fd_,
          dlocks_,
          s,
          boost::make_function_output_iterator(
            [&](state_with_data<box_state, backtrack_data> const & tgt) {
              bool frozen = is_frozen_deadlock(
                fd_,
                dlocks_,
                tgt.key.boxes(),
                boost::begin(tgt.key.boxes()) + tgt.data.box_diff);
              if (!frozen)
                *out++ = tgt;
            }));
  }
};

using point_collection = std::unordered_set<point>;

struct packing_sequence_state {
  boost::dynamic_bitset<> controlled_boxes;
  std::ptrdiff_t packing_step_id;
};

using packing_sequence = std::vector<packing_step>;

struct packing_sequence_push_transition {
  using state = hashable_data<box_state, packing_sequence_state>;

private:
  field_descriptor const & fd_;
  packing_sequence const & packing_;
  simple_deadlock_set const & dlocks_;

public:
  packing_sequence_push_transition(
          field_descriptor const & fd,
          packing_sequence const & ps,
          simple_deadlock_set const & dlocks)
      : fd_(fd)
      , packing_(ps)
      , dlocks_(dlocks) {
  }


  template<class OutIt>
  void operator()(state const & st, OutIt out) {
    auto st_data = st.data;

    assert(st_data.packing_step_id < packing_.size());
    auto step = packing_[st_data.packing_step_id];

    bool found_packing_push = false;
    for (auto box_it = boost::begin(st.key.boxes()),
              end_it = boost::end(st.key.boxes());
         box_it != end_it;
         ++box_it) {
      auto box_dist = std::distance(boost::begin(st.key.boxes()), box_it);

      if (step.forced && step.from != *box_it)
        continue;

      if (st_data.controlled_boxes[box_dist] && !step.forced)
        continue;

      direct_transitions_ex<true>(fd_, dlocks_, st.key, box_it,
          [&](point new_box_pos) {
            return new_box_pos == step.to;
          },
          boost::make_function_output_iterator(
            [&](state_with_data<box_state, backtrack_data> const & res) {
              packing_sequence_state pss = st_data;
              pss.controlled_boxes[box_dist] = true;
              ++pss.packing_step_id;

              found_packing_push = true;
              backtrack_data data = res.data;
              data.penalty = 0;
              *out++ = state_with_data<state, backtrack_data>({res.key, pss}, res.data);
            }
          )
      );
    }

    //std::cout << "Packing push: " << found_packing_push << std::endl;
    if (!found_packing_push)
      simple_push_transitions(fd_, dlocks_, st.key,
          boost::make_function_output_iterator(
            [&](state_with_data<box_state, backtrack_data> const & s) {
              if (st.data.controlled_boxes[s.data.box_diff])
                return;
              *out++ = state_with_data<state, backtrack_data>({s.key, st_data}, s.data);
            })
      );
  }
};

template<class Trans>
struct weighted_transition {
private:
  Trans inner_;

public:
  weighted_transition(Trans tr)
      : inner_(tr){
  }

public:
  template<class State, class OutIt>
  void operator()(State const & st, OutIt out) {
    inner_(st, boost::make_function_output_iterator(
        [&](State const & st) {
          *out++ = edge_type<State>{st, 1};
        }));
  }
};

template<class Trans>
weighted_transition<Trans> make_weighted_transition(Trans tr) {
  return weighted_transition<Trans>(tr);
}


std::tuple<mark_ptr, box_state> search_with_packing_sequence(
      field_descriptor const & fd,
      box_state const & initial,
      packing_sequence const & ps,
      simple_deadlock_set const & dlocks) {

  using mstate = marked_state<packing_sequence_push_transition::state, backtrack_data>;
  mstate source = {
    {initial,
      {boost::dynamic_bitset<>(boost::size(initial.boxes())), 0}},
    nullptr};

  std::array<marked_state<packing_sequence_push_transition::state, backtrack_data>, 1>
    srcs{{source}};

  auto trans = make_marking_transition(
      packing_sequence_push_transition(fd, ps, dlocks));

  mark_ptr res = nullptr;
  box_state res_state = initial;
  auto vis = sokoban::search::detail::make_func_visitor(
      [&](marked_state<packing_sequence_push_transition::state, backtrack_data> const & st,
          int dist) {
        /* Debug output */
        /*
        std::cout << "Distance " << dist << std::endl;
        std::cout << "Step " << st.inner_state.data.packing_step_id << std::endl;

        std::cout << "Boxes under control: " << st.inner_state.data.controlled_boxes
        << std::endl;

        print(fd, st.inner_state.key, std::cout);*/
        if (st.inner_state.data.packing_step_id == ps.size()) {
          res = st.parent_mark;
          res_state = st.inner_state.key;
          return open_result::StopSearch;
        }
        return open_result::ContinueSearch;
      }
  );

  auto manhattan_heuristics = [&](mstate const & st) {
    int sum = 0;

    for (auto box : st.inner_state.key.boxes()) {
      auto min = std::numeric_limits<int>::max();
      for (auto goal : fd.goals()) {
        int val = std::abs(std::get<0>(goal) - std::get<0>(box))
          + std::abs(std::get<1>(goal) - std::get<1>(box));
        if (val < min)
          min = val;
      }
      sum += min;
    }

    return 1000 * st.inner_state.data.packing_step_id + sum;
  };
  auto weigh_fun = [&](int astar_score, mstate const & st) {
    if (astar_score < 0)
      return 0;
    return astar_score + 3;
  };
  astar(srcs, trans, vis, weigh_fun, manhattan_heuristics);
  return std::make_tuple(res, res_state);
}


int main(int argc, char const* argv[]) {
  std::ifstream file(argv[1]);
  std::noskipws(file);
  field f = read_field(std::istream_iterator<char>(file), std::istream_iterator<char>(),
      &state_by_char, [](char c) { return c == '\n'; });
  file.close();

  sokoban::search::field_descriptor fd = descriptor_from_field(f);
  simple_deadlock_set dlocks = create_deadlock_set(fd);
  box_state initial = initial_pos(fd, f);

  std::vector<packing_step> packing;
  if (!find_packing_sequence(fd, initial_pos(fd, f), std::back_inserter(packing)))
    std::cout << "X" << std::endl;
  else {
    /*
    std::cout << "Found packing sequence with " << packing.size() << " steps" << std::endl;
    boost::copy(packing, std::ostream_iterator<packing_step>(std::cout)); */

    mark_ptr res;
    box_state final_state = initial;
    std::tie(res, final_state) = search_with_packing_sequence(fd, initial, packing, dlocks);
    if (!res)
      std::cout << "X" << std::endl;
    else
    {
      std::vector<char> path;
      restore_path(fd, res, final_state,
          boost::make_function_output_iterator([&](direction dir) {
            switch (dir) {
              case direction::Left:
                path.push_back('L');
                break;
              case direction::Up:
                path.push_back('U');
                break;
              case direction::Down:
                path.push_back('D');
                break;
              case direction::Right:
                path.push_back('R');
                break;
              default:
                path.push_back('?');
            }
      }));

      boost::copy(path | boost::adaptors::reversed, std::ostream_iterator<char>(std::cout));
      std::cout << std::endl;
    }
  }

  return 0;
}
