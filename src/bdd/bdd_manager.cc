#include "sokos_defs.h"

#include "bdd_manager.h"

#include <bdd.h>

using namespace bdd_util;

buddy_package::buddy_package(int nodeNum) {
  bdd_init(nodeNum, 10000);

  #ifndef SOKOS_DEBUG
  bdd_gbc_hook(nullptr);
  #endif
}

void buddy_package::set_cache_ratio(int ratio) {
  bdd_setcacheratio(ratio);
}

void buddy_package::set_max_increase(int maxIncrNodes) {
  bdd_setmaxincrease(maxIncrNodes);
}


buddy_package::~buddy_package() {
#ifdef SOKOS_DEBUG
  bddCacheStat stat;
  bdd_cachestats(&stat);

  std::cout << "Unique access: " << stat.uniqueAccess << "\n";
  std::cout << "Unique chain:  " << stat.uniqueChain << "\n";
  std::cout << "Unique hit:    " << stat.uniqueHit << "\n";
  std::cout << "Unique miss:   " << stat.uniqueMiss << "\n";
  std::cout << "Op hit:        " << stat.opHit << "\n";
  std::cout << "Op miss:       " << stat.opMiss << "\n";
  std::cout << "Swap count:    " << stat.swapCount << "\n";
#endif

  bdd_done();
}


std::shared_ptr<bddPair> bdd_util::make_bdd_pair() {
  return std::shared_ptr<bddPair>(bdd_newpair(), &bdd_freepair);
}
