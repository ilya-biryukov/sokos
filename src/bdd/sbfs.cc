#include "sokos_defs.h"

#include "sbfs.h"

#include <fdd.h>

#include <cassert>
#include <algorithm>

using namespace bdd_util;

bool sbfs_engine::is_reachable() const {
  return is_reachable_;
}

void sbfs_engine::run() {
  // TODO: status-reporting
  all_transitions_ = bddfalse;
  for (auto t : transitions_) {
    all_transitions_ |= t & !bdd_replace(pruned_, main_to_other_.get());
  }

  SOKOS_LOG_DEBUG("Trying to solve the level");

  std::vector<bdd> all_frontiers;

  auto visited = bddfalse;
  auto frontier = start_;
  int iter_ctr = 0;
  for (auto frontier = start_, visited = bddfalse;
        frontier != bddfalse;
        visited |= frontier, frontier = iterate_and_return_next(frontier) &! visited) {
    ++iter_ctr;

    SOKOS_LOG_DEBUG("Iteration: " << iter_ctr);

    all_frontiers.push_back(frontier);
    auto goals = frontier & goal_;
    if (goals != bddfalse) {
      is_reachable_ = true;
      restore_path(visited, all_frontiers, iter_ctr - 1);
      return;
    }
  }

  is_reachable_ = false;
}

void sbfs_engine::restore_path(bdd visited, std::vector<bdd> const & frontiers, int iter_ctr) {
  path_.resize(iter_ctr, -1);

  SOKOS_LOG_DEBUG("Restoring path...");

  auto frontier = frontiers[iter_ctr];
  assert((visited & frontier) == bddfalse);

  auto goals = frontier & goal_;
  assert(goals != bddfalse);

  auto walker = bdd_satoneset(goals, main_vars_, bddfalse);
  for (int n = iter_ctr - 1; n >= 0; --n) {
    SOKOS_LOG_DEBUG("Iteration " << n + 1);

    frontier = frontiers[n];
    assert((visited & frontier) == frontier);

    visited &= !frontier;
    assert((visited & frontier) == bddfalse);
    assert((walker & frontier) == bddfalse);

    auto walker_tmp = bdd_replace(walker, main_to_other_.get());
    for (size_t tr_id = 0; tr_id < transitions_.size(); ++tr_id) {
      auto t = transitions_[tr_id];
      auto prev = bdd_relprod(walker_tmp, t, other_vars_) & frontier;
      if (prev != bddfalse) {
        prev = bdd_satoneset(prev, main_vars_, bddfalse);
        path_[n] = tr_id;
        walker = prev;
        break;
      }
      path_[n] = tr_id;
    }
    assert(path_[n] != -1);
  }

  assert(frontier == start_);
  assert(visited == bddfalse);
}

bdd sbfs_engine::iterate_and_return_next(bdd it) {
  auto rel = bdd_relprod(all_transitions_, it, main_vars_);

  return bdd_replace(rel, other_to_main_.get());
}
