#pragma once

#include <bdd.h>

#include <vector>
#include <initializer_list>
#include <memory>
#include <boost/range/iterator_range.hpp>

namespace bdd_util {

  struct sbfs_engine {
    template<class InputRange>
    sbfs_engine(bdd start,
            bdd goal,
            bdd main_vars,
            bdd other_vars,
            bdd temp_vars,
            std::shared_ptr<bddPair> main_to_other,
            std::shared_ptr<bddPair> other_to_main,
            std::shared_ptr<bddPair> main_to_temp,
            std::shared_ptr<bddPair> other_to_temp,
            InputRange transitions,
            bdd pruned_states)
        : start_(start)
        , goal_(goal)
        , main_vars_(main_vars)
        , other_vars_(other_vars)
        , temp_vars_(temp_vars)
        , transitions_(boost::copy_range<std::vector<bdd>>(transitions))
        , main_to_other_(main_to_other)
        , other_to_main_(other_to_main)
        , other_to_temp_(other_to_temp)
        , main_to_temp_(main_to_temp)
        , pruned_(pruned_states) {
      run();
    }

    bool is_reachable() const;

    template<class OutIter>
    void output_path(OutIter it) {
      std::copy(path_.begin(), path_.end(), it);
    }
  private:
    void run();
    void restore_path(bdd visited, std::vector<bdd> const & frontiers, int iter_ctr);
    bdd iterate_and_return_next(bdd it);

  private:
    bdd start_;
    bdd goal_;

    bdd main_vars_;
    bdd other_vars_;
    bdd temp_vars_;

    std::vector<bdd> transitions_;
    bdd all_transitions_;

    std::shared_ptr<bddPair> main_to_other_;
    std::shared_ptr<bddPair> other_to_main_;
    std::shared_ptr<bddPair> other_to_temp_;
    std::shared_ptr<bddPair> main_to_temp_;
    bdd pruned_;

    bool is_reachable_;
    std::vector<int> path_;
  };

} /* bdd_util */
