#pragma once

#include <memory>
#include <bdd.h>

namespace bdd_util {
  struct buddy_package {
    buddy_package(int nodeNum);

    void set_cache_ratio(int ratio);
    void set_max_increase(int maxIncrNodes);

    ~buddy_package();
  };

  std::shared_ptr<bddPair> make_bdd_pair();
} /* bdd */
