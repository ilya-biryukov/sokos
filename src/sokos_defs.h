#pragma once


#ifdef SOKOS_DEBUG

#include <iostream>
#define SOKOS_LOG_DEBUG(a) std::cout << a << std::endl;

#else

#define SOKOS_LOG_DEBUG(a)

#endif
