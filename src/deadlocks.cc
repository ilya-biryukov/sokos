#include "sokos_defs.h"

#include "deadlocks.h"

#include <boost/unordered_map.hpp>

namespace {
  template<class Fun>
  struct call_on_examine_visitor : boost::bfs_visitor<> {

    call_on_examine_visitor(Fun fn)
        : fun_(fn) {
    }

    void examine_vertex(classic_search::field_graph::vertex_descriptor v,
        classic_search::field_graph const &) {
      fun_(v);
    }

  private:
    Fun fun_;
  };

  template<class Fun>
  call_on_examine_visitor<Fun> make_call_visitor(Fun fn) {
    return call_on_examine_visitor<Fun>(fn);
  }


  classic_search::field_descriptor descriptor_from_field(sokoban::field const & f) {
    using namespace sokoban;
    using namespace classic_search;

    std::vector<point> walls;
    for (std::size_t x = 0; x < f.width(); ++x) {
      for (std::size_t y = 0; y < f.height(); ++y) {
        if (f(x, y) == cell_state::Wall) {
          walls.emplace_back((int)x, (int)y);
        }
      }
    }

    std::array<point, 0> goals; // they don't here

    return field_descriptor(f.width(), f.height(), walls, goals);
  }

  template<class Graph, class InputRange, class Visitor>
  void breadth_first_visit(
      Graph const & g,
      deadlocks::detail::color_map_type & colors,
      InputRange sources,
      Visitor vis) {
    typedef boost::queue<typename boost::graph_traits<Graph>::vertex_descriptor>
      queue_type;

    queue_type q;
    boost::associative_property_map<deadlocks::detail::color_map_type> color_map(colors);

    return boost::breadth_first_visit(g, boost::begin(sources), boost::end(sources),
        q, vis, color_map);
  }
} /*  */

void deadlocks::detail::run_search(
        sokoban::field const & f,
        sokoban::field::coordinate_type start,
        boost::unordered_set<sokoban::field::coordinate_type> & res,
        color_map_type & color_map) {
  using namespace classic_search;
  field_descriptor fd = descriptor_from_field(f);

  std::array<point, 4> diffs = {{{1, 0}, {-1, 0}, {0, 1}, {0, -1}}};
  std::vector<state<point>> sources;
  for (auto && df : diffs) {
    point pos = start;
    std::get<0>(pos) += std::get<0>(df);
    std::get<1>(pos) += std::get<1>(df);

    if (can_place_at(fd, pos)) {
      sources.emplace_back(pos, std::array<point, 1>{{start}});
    }
  }

  boost::unordered_set<sokoban::field::coordinate_type>& non_deadlocks = res;
  field_graph fg(fd);
  breadth_first_visit(
      fg,
      color_map,
      sources,
      make_call_visitor(
        [&non_deadlocks](state<point> const & p) {
          non_deadlocks.insert(*boost::begin(p.boxes()));
        }
      )
  );
}
