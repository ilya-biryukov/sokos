#pragma once

#include <vector>
#include <cassert>

#include "util/hash_util.h"

namespace util {
  using std::size_t;

  template<class T>
  struct array2d {
    array2d(size_t width, size_t height)
        : elements_(width * height, {})
        , width_(width) {
    }

    template<class InIter>
    array2d(size_t width, size_t height, InIter begin, InIter end)
        : elements_(begin, end)
        , width_(width) {
      assert(elements_.size() == width * height && "range passed didn't match in size");
    }

    typedef T value_type;
    typedef T& reference;
    typedef T const& const_reference;

    typedef typename std::vector<T>::iterator iterator;
    typedef typename std::vector<T>::const_iterator const_iterator;

    typedef std::pair<size_t, size_t> coordinate_type;

    size_t size() const {
      return elements_.size();
    }

    size_t width() const {
      return width_;
    }

    size_t height() const {
      return size() / width();
    }

    reference operator()(size_t x, size_t y) {
      assert(x < width() && "x is too large");
      assert(y < height() && "y is too large");

      return elements_[x + y * width_];
    }

    const_reference operator()(size_t x, size_t y) const {
      return const_cast<array2d&>(*this)(x, y);
    }

    reference operator()(coordinate_type c) {
      return (*this)(c.first, c.second);
    }

    const_reference operator()(coordinate_type c) const {
      return (*this)(c.first, c.second);
    }

    iterator begin() {
      return elements_.begin();
    }

    iterator end() {
      return elements_.end();
    }

    const_iterator begin() const {
      return elements_.begin();
    }

    const_iterator end() const {
      return elements_.end();
    }

  private:
    std::vector<T> elements_;
    std::size_t width_;
  };
} /* util */

namespace std {
  template<class T>
  struct hash<util::array2d<T>> {
    std::size_t operator()(util::array2d<T> const & arr) {
      return 27 * 27 * std::hash<std::size_t>()(arr.width())
        + 27 * std::hash<std::size_t>()(arr.height())
        + util::hash_seq(arr.begin(), arr.end());
    }
  };
} /* std */
