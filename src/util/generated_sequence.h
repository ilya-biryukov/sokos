#pragma once

namespace util {
  template<class Gen>
  struct generated_sequence {

  private:
    int var_num(coord_type c) {
      return c.first + c.second *fld_.width() + first_fdd_var_;
    }

  };
} /* util */
