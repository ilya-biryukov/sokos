#pragma once

#include <functional>
#include <iterator>

namespace util {
  template<class InputIter>
  std::size_t hash_seq(InputIter beg, InputIter end) {
    typedef std::hash<typename std::iterator_traits<InputIter>::value_type> hash_type;

    std::size_t res = 0;
    for (auto it = beg; it != end; ++it) {
      res = 37 * res + hash_type()(*it);
    }

    return res;
  }
} /* util */

namespace std {
  template<class T, class U>
  struct hash<std::pair<T, U>> {
    std::size_t operator()(std::pair<T, U> const & arg) const {
      return 61 * std::hash<T>()(arg.first) + std::hash<U>()(arg.second);
    }
  };
} /* std */
