#include <utility>

namespace util { 
  template<class Iter>
  struct iterator_range : std::pair<Iter, Iter> {
    template<class... Args>
    iterator_range(Args... a)
      : std::pair<Iter, Iter>(a...)
    {}


    Iter begin() const {
      return this->first;
    }

    Iter end() const {
      return this->second;
    }
  };
  

  template<class Iter>
  iterator_range<Iter> make_range(Iter beg, Iter end) {
    return iterator_range<Iter>(beg, end);
  }


} /* util */
