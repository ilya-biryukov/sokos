#include "sokos_defs.h"

#include "solver.h"

#include <array>
#include <iterator>

#include <bdd.h>
#include <fdd.h>

#include "bdd/bdd_manager.h"
#include "bdd/sbfs.h"
#include "deadlocks.h"
#include "boost/unordered_set.hpp"

using namespace sokos;

namespace {
  struct generate_solver {
    typedef sokoban::field::coordinate_type coord_type;

    generate_solver(sokoban::field const & fld)
        : fld_(fld) {
      init_bdd_domains();
      field_eq_bdd_ = gen_field_eq_bdd_impl();

      SOKOS_LOG_DEBUG("computing simple deadlocks");
      deadlocks::collect_simple_deadlock_positions(fld_,
          std::inserter(deadlocks_, deadlocks_.begin()));
    }

    ~generate_solver() {
      fdd_clearall();
    }

    bdd generate_move_bdd(coord_type pers_c, int dx, int dy) {
      using namespace sokoban;

      //TODO: handle 'teleports' at the edges
      coord_type other_c = coord_type(pers_c.first + dx, pers_c.second + dy);

      if (!can_place(pers_c) || !can_place(other_c))
        return bddfalse;

      bdd eq_bdd = gen_field_eq_bdd(std::initializer_list<coord_type>{pers_c, other_c});

      return eq_bdd
        & fdd_ithvar(main_var_num(pers_c), int_state(cell_state::Player))
        & fdd_ithvar(main_var_num(other_c), int_state(cell_state::Empty))

        & fdd_ithvar(other_var_num(pers_c), int_state(cell_state::Empty))
        & fdd_ithvar(other_var_num(other_c), int_state(cell_state::Player));
    }

    template<class Gen>
    bdd generate_move_bdd_impl(int dx, int dy, Gen g) {
      bdd res = bddfalse;
      for (size_t y = fld_.height(); y > 0; --y) {
        for (size_t x = fld_.width(); x > 0; --x) {
          res |= g(coord_type(x, y), dx, dy);
        }
      }

      return res;
    }

    bdd generate_move_bdd(int dx, int dy) {
      return generate_move_bdd_impl(dx, dy,
          [this](coord_type c, int dx, int dy) { return generate_move_bdd(c, dx, dy); });
    }

    bdd generate_push_move_bdd(int dx, int dy) {
      return generate_move_bdd_impl(dx, dy,
          [this](coord_type c, int dx, int dy) {
            return generate_push_move_bdd(c, dx, dy);
      });
    }

    bdd generate_push_move_bdd(coord_type pers_c, int dx, int dy) {
      using namespace sokoban;

      //TODO: handle 'teleports' at the edges
      coord_type box_c = coord_type(pers_c.first + dx, pers_c.second + dy);
      coord_type empty_c = coord_type(pers_c.first + 2 * dx, pers_c.second + 2 * dy);

      if (!can_place(pers_c) || !can_place(box_c) || !can_place(empty_c)
          || deadlocks_.find(box_c) != deadlocks_.end()
          || deadlocks_.find(empty_c) != deadlocks_.end())
        return bddfalse;

      bdd eq_bdd = gen_field_eq_bdd(std::initializer_list<coord_type>{
          pers_c, box_c, empty_c
      });

      return eq_bdd
        & fdd_ithvar(main_var_num(pers_c), int_state(cell_state::Player))
        & fdd_ithvar(main_var_num(box_c), int_state(cell_state::Box))
        & fdd_ithvar(main_var_num(empty_c), int_state(cell_state::Empty))

        & fdd_ithvar(other_var_num(pers_c), int_state(cell_state::Empty))
        & fdd_ithvar(other_var_num(box_c), int_state(cell_state::Player))
        & fdd_ithvar(other_var_num(empty_c), int_state(cell_state::Box));
    }

    solver_result solve() {
      using namespace bdd_util;
      // right, left, down, up
      SOKOS_LOG_DEBUG("generating transitions");
      std::array<bdd, 8> transitions{{
        generate_move_bdd(1,  0), generate_push_move_bdd(1,  0), // right
        generate_move_bdd(-1, 0), generate_push_move_bdd(-1, 0), // left
        generate_move_bdd(0,  1), generate_push_move_bdd(0,  1), // up
        generate_move_bdd(0, -1), generate_push_move_bdd(0, -1)  // down
      }};
      SOKOS_LOG_DEBUG("sat-count: "
        << bdd_satcountset(transitions[0],
            fdd_makeset(main_numbers_.data(), main_numbers_.size()))
        << std::endl);

      std::shared_ptr<bddPair> main_to_other = gen_vars_to_vars_pairs(
          main_numbers_.data(), other_numbers_.data());
      std::shared_ptr<bddPair> other_to_main = gen_vars_to_vars_pairs(
          other_numbers_.data(), main_numbers_.data());
      std::shared_ptr<bddPair> main_to_temp = gen_vars_to_vars_pairs(
          main_numbers_.data(), temp_numbers_.data());
      std::shared_ptr<bddPair> other_to_temp = gen_vars_to_vars_pairs(
          other_numbers_.data(), temp_numbers_.data());

      SOKOS_LOG_DEBUG("Running search");
      sbfs_engine bfs = sbfs_engine(
          gen_start(),
          gen_goal(),
          fdd_makeset(main_numbers_.data(), main_numbers_.size()),
          fdd_makeset(other_numbers_.data(), other_numbers_.size()),
          fdd_makeset(temp_numbers_.data(), temp_numbers_.size()),
          main_to_other,
          other_to_main,
          main_to_temp,
          other_to_temp,
          transitions,
          /*gen_prune_set_by_deadlocks(deadlocks_)*/
          bddfalse);

      std::vector<int> moves;
      bfs.output_path(std::back_inserter(moves));

      std::vector<move> moves2;
      SOKOS_LOG_DEBUG("Moves");

      for (auto x : moves) {
        SOKOS_LOG_DEBUG(x);

        moves2.push_back((move)x);
      }

      return {bfs.is_reachable(), std::move(moves2)};
    }

  private:
    int const kDomainSize = 3; // person, empty, box

    template<class InputRange>
    bdd gen_prune_set_by_deadlocks(InputRange deadlocks) {
      auto res = bddfalse;
      for (auto c : deadlocks) {
        res |= fdd_ithvar(main_var_num(c), int_state(sokoban::cell_state::Box));
      }

      return res;
    }

    int int_state(sokoban::cell_state cs) {
      using sokoban::cell_state;

      switch(cs) {
        case cell_state::Empty:
          return 0;
        case cell_state::Box:
          return 1;
        case cell_state::Player:
          return 2;
        default:
          assert(false && "bad value for cell_state");
      }
      return 0;
    }

    bdd gen_field_eq_bdd_impl() {
      using sokoban::cell_state;
      bdd res = bddtrue;
      for (size_t y = fld_.height(); y > 0; --y) {
        for (size_t x = fld_.width(); x > 0; --x) {
          auto c = coord_type(x-1, y-1);
          auto lv = main_var_num(c);
          auto rv = other_var_num(c);
          res &= (
              (fdd_ithvar(lv, int_state(cell_state::Empty))
                | fdd_ithvar(rv, int_state(cell_state::Box)))
              & fdd_equals(lv, rv)
          );
        }
      }

      return res;
    }

    template<class InputRange>
    bdd gen_field_eq_bdd(InputRange holes) {
      auto hole_vars = bddtrue;
      for (auto h : holes) {
        hole_vars &= fdd_ithset(main_var_num(h));
        hole_vars &= fdd_ithset(other_var_num(h));
      }

      return bdd_exist(field_eq_bdd_, hole_vars);
    }

    bool can_place(coord_type c) {
      if (c.first >= fld_.width() || c.second >= fld_.height())
        return false;
      return fld_(c) != sokoban::cell_state::Wall;
    }

    void init_bdd_domains() {
      std::array<int, 1> domainSize{{kDomainSize}};
      first_fdd_var_ = fdd_extdomain(domainSize.data(), 1);

      for (size_t i = 1; i < 3 * fld_.width() * fld_.height(); ++i) {
        fdd_extdomain(domainSize.data(), 1);
      }


      // Init helper structure
      size_t vars = fld_.width() * fld_.height();
      main_numbers_.resize(vars);
      other_numbers_.resize(vars);
      temp_numbers_.resize(vars);

      for (size_t i = 0; i < vars; ++i) {
        main_numbers_[i] = first_fdd_var_ + 3 * i;
        other_numbers_[i] = first_fdd_var_ + 3 * i + 2;
        temp_numbers_[i] = first_fdd_var_ + 3 * i + 1;
      }
    }

    int main_var_num(coord_type c) {
      assert(c.first < fld_.width() && c.second < fld_.height()
          && "x or y coord is too large");

      return first_fdd_var_ + 3 * (c.first + c.second * fld_.width());
    }

    int other_var_num(coord_type c) {
      assert(c.first < fld_.width() && c.second < fld_.height()
          && "x or y coord is too large");

      return first_fdd_var_ + 3 * (c.first + c.second * fld_.width()) + 2;
    }

    std::shared_ptr<bddPair> gen_vars_to_vars_pairs(int* vars1, int* vars2) {
      size_t vars = fld_.width() * fld_.height();

      std::shared_ptr<bddPair> res = bdd_util::make_bdd_pair();
      fdd_setpairs(res.get(), vars1, vars2, vars);
      return res;
    }

    bdd gen_goal() {
      using namespace sokoban;

      bdd res = bddtrue;

      for (size_t x = 0; x < fld_.width(); ++x) {
        for (size_t y = 0; y < fld_.height(); ++y) {
          if (fld_(x, y) == cell_state::Target || fld_(x, y) == cell_state::BoxTarget
                || fld_(x,y) == cell_state::PlayerTarget) {
            res &= fdd_ithvar(main_var_num(coord_type(x, y)),
                int_state(cell_state::Box));
          }
        }
      }
      return res;
    }

    bdd gen_start() {
      bdd res = bddtrue;

      for (size_t x = 0; x < fld_.width(); ++x) {
        for (size_t y = 0; y < fld_.height(); ++y) {
          res &= fdd_ithvar(
              main_var_num(coord_type(x, y)),
              clear_unwanted_state(fld_(x, y)));
        }
      }

      return res;
    }

    int clear_unwanted_state(sokoban::cell_state s) {
      using sokoban::cell_state;

      switch (s) {
        case cell_state::BoxTarget:
          s = cell_state::Box;
          break;
        case cell_state::PlayerTarget:
          s = cell_state::Player;
          break;
        case cell_state::Wall:
        case cell_state::Target:
          s = cell_state::Empty;
          break;
        default:
          break;
      }

      return int_state(s);
    }
  private:
    std::vector<int> main_numbers_;
    std::vector<int> other_numbers_;
    std::vector<int> temp_numbers_;
    boost::unordered_set<coord_type> deadlocks_;
    sokoban::field const& fld_;
    bdd field_eq_bdd_;
    int first_fdd_var_;
  };
}

solver_result sokos::solve_field(sokoban::field f) {
  generate_solver solver(f);
  return solver.solve();
}

