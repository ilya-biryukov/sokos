#pragma once

#include "util/array2d.h"
#include "util/iterator_range.h"

namespace sokoban {
  using std::size_t;

  enum class cell_state {
    Empty = 0,
    Player = 1,
    Box = 2,
    Wall,
    Target,
    BoxTarget,
    PlayerTarget
  };

  typedef util::array2d<cell_state> field;

  inline bool is_target(cell_state cs) {
    return cell_state::Target <= cs && cs <= cell_state::PlayerTarget;
  }

  template<class InIter, class Func, class IsRowDelimPred>
  field read_field(InIter begin, InIter end, Func f, IsRowDelimPred pred) {
    std::vector<std::vector<cell_state>> cells;

    size_t current_width = 0;
    size_t width = 0;
    size_t height = 0;
    cells.emplace_back();
    for (auto c : util::make_range(begin, end)) {
      if (pred(c)) {
        ++height;
        cells.emplace_back();

        width = std::max(current_width, width);
        current_width = 0;
      } else {
        ++current_width;
        cells[height].emplace_back(f(c));
      }
    }

    assert(height != 0 && "height is 0");
    assert(width != 0 && "width is 0");

    field fld(width, height);
    for (size_t y = 0; y < height; ++y) {
      for (size_t x = 0; x < width; ++x) {
        if (x < cells[y].size()) {
          fld(x, y) = cells[y][x];
        } else {
          fld(x, y) = cell_state::Empty;
        }
      }
    }
    return fld;
  }

  template<class OutIter>
  void write_field(field const & fld, OutIter) {
    for (size_t y = 0; y < fld.height(); ++y) {
      for (size_t x = 0; y < fld.width(); ++x) {

      }
    }
  }

} /* sokoban  */

namespace std {
  template<>
  struct hash<sokoban::cell_state> {
    std::size_t operator()(sokoban::cell_state s) const{
      return std::hash<int>()(static_cast<int>(s));
    }
  };
} /* std */
