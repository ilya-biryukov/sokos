#pragma once

#include <memory>
#include <vector>
#include "sokoban/search/path_mark.hpp"
#include "sokoban/search/transitions.hpp"
#include "sokoban/search/box_state.hpp"
#include "sokoban/search/dfs.hpp"
#include "sokoban/search/printers.hpp"

namespace sokoban { namespace search {

  using mark_ptr = std::shared_ptr<path_mark<backtrack_data>>;

  namespace detail {

    // return player position
    inline point pull(std::vector<point> & boxes, int box_n, direction pull_dir) {
      assert(box_n < boxes.size());

      auto& box_pos = boxes[box_n];

      auto fwd = adj_point_fwd(box_pos, pull_dir);
      box_pos = fwd;

      return adj_point_fwd(fwd, pull_dir);
    }

    // return player position
    inline point push(std::vector<point> & boxes, int box_n, direction push_dir) {
      assert(box_n < boxes.size());

      auto& box_pos = boxes[box_n];

      point old_box = box_pos;
      auto fwd = adj_point_fwd(box_pos, push_dir);
      box_pos = fwd;

      return old_box;
    }

    // return player position
    inline point reverse_push(std::vector<point>& boxes, int box_n, direction push_dir) {
      return pull(boxes, box_n, opposite_direction(push_dir));
    }
    inline point reverse_pull(std::vector<point>& boxes, int box_n, direction pull_dir) {
      return push(boxes, box_n, opposite_direction(pull_dir));
    }

    template<class OutIt>
    void restore_player_path(
          field_descriptor const & fd,
          std::vector<point> const & boxes,
          point pos,
          point new_pos,
          OutIt out) {
      if (pos == new_pos)
        return;

      auto transition = make_marking_transition(
          make_move_player_transition<false>(fd, boxes));

      using state = decltype(transition)::marked_state<point, direction>;

      std::shared_ptr<path_mark<direction>> res;

      class visitor {
        point target_;
        std::shared_ptr<path_mark<direction>> & result_;

      public:
        visitor(point target, std::shared_ptr<path_mark<direction>> & result)
            : target_(target)
            , result_(result) {
        }


        open_result open_vertex(state const & state) {
          if (state.inner_state == this->target_)
          {
            this->result_ = state.parent_mark;
            return open_result::StopSearch;
          }

          return open_result::ContinueSearch;
        }
      };

      std::array<state, 1> sources{{state{pos, {nullptr}}}};
      dfs(sources, transition, visitor(new_pos, res));

      assert(res);

      for (auto current = res; current; current = current->parent()) {
        *out++ = current->data();
      }
    }

    inline point player_after_push(point box, direction push_dir) {
      return adj_point_bwd(box, push_dir);
    }

    inline point player_after_pull(point box, direction pull_dir) {
      return adj_point_fwd(box, pull_dir);
    }

    template<class OutIt>
    void restore_box_path(
          field_descriptor const & fd,
          std::vector<point> & boxes,
          point & player,
          backtrack_data const & bd,
          OutIt out) {
      assert(boxes[bd.box_diff] == bd.new_box_pos);

      std::vector<point> other_boxes = boost::copy_range<std::vector<point>>(
            boxes | boost::adaptors::filtered([&](point pt) {
                return pt != bd.new_box_pos;
      }));

      simple_deadlock_set empty_dlocks = deadlocks::create_empty_set();
      auto transition = make_marking_transition(
            detail::move_single_box_transition<false, std::vector<point>>(
              fd, empty_dlocks, other_boxes)
      );

      using state = decltype(transition)::marked_state<box_state, backtrack_data>;

      std::shared_ptr<path_mark<backtrack_data>> res;
      auto vis = detail::make_func_visitor([&](state const & state) {
          assert(boost::size(state.inner_state.boxes()) == 1);

          auto box = *boost::begin(state.inner_state.boxes());

          if (box == bd.old_box_pos
              && state.inner_state.positions().is_reachable(bd.old_player_point)) {
            res = state.parent_mark;
            return open_result::StopSearch;
          }
          return open_result::ContinueSearch;
      });

      auto box = boxes[bd.box_diff];
      auto box_state = make_single_box_state(fd, box,
          compute_cached_positions(fd, boxes, player), other_boxes);

      {
       std::array<state, 1> sources{{state{box_state, nullptr}}};
       dfs(sources, transition, vis);
      }

      assert(res);
      {
        auto init_pl
          = player_after_pull(res->data().new_box_pos, res->data().last_push_direction);
        auto pl = init_pl;

        boxes[bd.box_diff] = bd.old_box_pos;

        std::vector<direction> pulls;
        for (auto current = res; current; current = current->parent()) {
          auto const & data = current->data();
          auto pull_dir = data.last_push_direction;

          auto new_pl_pos = player_after_pull(data.new_box_pos, pull_dir);
          restore_player_path(fd, boxes, new_pl_pos, pl, std::back_inserter(pulls));

          pl = reverse_pull(boxes, bd.box_diff, pull_dir);
          pulls.push_back(pull_dir);
        }

        restore_player_path(fd, boxes, player, pl, std::back_inserter(pulls));
        player = init_pl;

        boost::copy(
            pulls | boost::adaptors::transformed(&opposite_direction) | boost::adaptors::reversed,
            out);
      }

      assert(boxes[bd.box_diff] == bd.new_box_pos);
      boxes[bd.box_diff] = bd.old_box_pos;
    }
  }


  template<class OutIt>
  void restore_path(
        field_descriptor const & fd,
        mark_ptr state_mark,
        box_state final_state,
        OutIt out) {

    if (!state_mark)
      return;

    auto boxes = boost::copy_range<std::vector<point>>(final_state.boxes());
    auto player = detail::player_after_push(
        state_mark->data().new_box_pos,
        state_mark->data().last_push_direction);

    for (auto current = state_mark; current; current = current->parent())
      detail::restore_box_path(fd, boxes, player, current->data(), out);

    detail::restore_player_path(fd, boxes, fd.initial_player(), player, out);
  }

  template<class BackData, class RestoreStep>
  void restore_path(
        std::shared_ptr<path_mark<BackData>> final_mark,
        RestoreStep rs) {
    for (auto current = final_mark; current; current = current->parent()) {
      rs(current->data());
    }
  }
}} /* sokoban::search */
