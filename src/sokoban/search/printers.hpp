#pragma once

#include "sokoban/search/field_descriptor.hpp"
#include "sokoban/search/box_state.hpp"
#include "deadlocks/simple.hpp"

namespace sokoban { namespace search {

template<class InputRange, class OutIt>
inline void print(field_descriptor const & fd, InputRange const& printers, OutIt out) {
  for (int y = 0; y < fd.height(); ++y) {
    for (int x = 0; x < fd.width(); ++x) {
      char c = 0;
      for (auto p : printers) {
        c = p(point(x, y));
        if (c)
          break;
      }
      if (!c)
        c = ' ';
      (*out++) = c;
    }
    (*out++) = '\n';
  }
}

inline void print(field_descriptor const & fd, box_state const & st, std::ostream& s) {
  auto state_printer = [&](point pt) -> char {
    if (st.positions().top_left() == pt)
    {
      if (fd.goal_at(pt))
        return '+';
      return '@';
    }
    if (boost::find(st.boxes(), pt) != boost::end(st.boxes()))
    {
      if (fd.goal_at(pt))
        return '*';
      return '$';
    }
    return 0;
  };

  auto field_printer = [&](point pt) -> char {
    if (fd.wall_at(pt))
      return '#';
    if (fd.goal_at(pt))
      return '.';
    return 0;
  };

  std::array<std::function<char(point)>, 2> printers = {{state_printer, field_printer}};

  print(fd, printers, std::ostream_iterator<char>(s));
}

inline void print(field_descriptor const & fd, simple_deadlock_set const & dl, std::ostream & s) {
  auto field_printer = [&](point pt) -> char {
    if (fd.wall_at(pt))
      return '#';
    return 0;
  };

  auto dl_printer = [&](point pt) -> char {
    if (dl.is_deadlocked(pt))
      return '~';
    return 0;
  };

  std::array<std::function<char(point)>, 2> printers = {{field_printer, dl_printer}};

  print(fd, printers, std::ostream_iterator<char>(s));
}

template<class BoxesRange>
inline void print(field_descriptor const & fd, BoxesRange const & boxes, point pl, std::ostream& s) {
  auto state_printer = [&](point pt) -> char {
    if (pl == pt)
    {
      if (fd.goal_at(pt))
        return '+';
      return '@';
    }
    if (boost::find(boxes, pt) != boost::end(boxes))
    {
      if (fd.goal_at(pt))
        return '*';
      return '$';
    }
    return 0;
  };

  auto field_printer = [&](point pt) -> char {
    if (fd.wall_at(pt))
      return '#';
    if (fd.goal_at(pt))
      return '.';
    return 0;
  };

  std::array<std::function<char(point)>, 2> printers = {{state_printer, field_printer}};

  print(fd, printers, std::ostream_iterator<char>(s));
}

}} /* sokoban::search */
