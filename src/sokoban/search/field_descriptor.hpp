#pragma once

#include <iterator>

#include "classic_search/field_descriptor.hpp"
#include "classic_search/state.hpp"
#include "classic_search/position.hpp"
#include "sokoban/search/direction.hpp"

namespace sokoban { namespace search {
  using point = classic_search::point;

  inline std::ostream & operator << (std::ostream & s, point const & pt) {
    return s << "(" << std::get<0>(pt) << ", " << std::get<1>(pt) << ")";
  }

  inline point adj_point_fwd(point const & p, direction dir) {
    auto x = std::get<0>(p);
    auto y = std::get<1>(p);
    switch (dir) {
      case direction::Up:
        return {x, y - 1};
      case direction::Down:
        return {x, y + 1};
      case direction::Left:
        return {x - 1, y};
      case direction::Right:
        return {x + 1, y};
      default:
        assert(false && "invalid direction");
        return {x, y};
    }
  }

  inline point adj_point_bwd(point const & p, direction dir) {
    return adj_point_fwd(p, opposite_direction(dir));
  }

  /* Field descriptor */
  using field_descriptor = classic_search::field_descriptor;

  /* Utility methods */
  template<class State>
  bool is_free(field_descriptor const & f, State const & st, point const & p) {
    return !f.wall_at(p) && !box_at(st, p);
  }

  template<class State>
  bool is_target(field_descriptor const & f, State const & st) {
    for (auto bx : st.boxes()) {
      if (!f.goal_at(bx))
        return false;
    }

    return true;
  }

  inline bool are_adjacent(point const & lhs, point const & rhs) {
    return (std::abs(std::get<0>(lhs) - std::get<0>(rhs)) +
      std::abs(std::get<1>(lhs) - std::get<1>(rhs))) == 1;
  }

  inline direction get_push_direction(point const & old_pos, point const & new_pos) {
    for (auto dir : all_directions())
      if (adj_point_fwd(old_pos, dir) == new_pos)
        return dir;

    assert("non-adjacent points");
    return direction::Right;
  }
}} /* sokoban::search */
