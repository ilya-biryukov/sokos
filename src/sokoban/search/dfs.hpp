#pragma once

#include <unordered_set>
#include <iterator>
#include <boost/operators.hpp>
#include <boost/function_output_iterator.hpp>
#include <boost/range/iterator.hpp>
#include "sokoban/search/path_mark.hpp"

namespace sokoban { namespace search {
  template<class Key, class Data>
  struct hashable_data : boost::equality_comparable<hashable_data<Key, Data>> {
    Key key;
    Data data;

    hashable_data() = default;

    hashable_data(Key const & k, Data const & d)
        : key(k)
        , data(d) {
    }

    friend bool operator ==(hashable_data const & lhs, hashable_data const & rhs) {
      return lhs.key == rhs.key;
    }
  };
}} /* sokoban::search */

namespace std {
  template<class Key, class Data>
  struct hash<sokoban::search::hashable_data<Key, Data>> {
    std::size_t operator()(sokoban::search::hashable_data<Key, Data> const & c) const {
      return std::hash<Key>()(c.key);
    }
  };
} /* std */

namespace sokoban { namespace search {
  template<class State, class Data>
  using state_with_data = hashable_data<State, Data>;

  template<class State, class Data>
  struct marked_state : private boost::equality_comparable<marked_state<State, Data>> {
    State           inner_state;
    std::shared_ptr<path_mark<Data>> parent_mark;

  public:
    marked_state(State const & inner, std::shared_ptr<path_mark<Data>> parent)
        : inner_state(inner)
        , parent_mark(parent) {
    }

    friend bool operator == (marked_state const & lhs, marked_state const & rhs) {
      return lhs.inner_state == rhs.inner_state;
    }
  };

  template<class Transition>
  struct marking_transition {
    using inner_transition = Transition;

    template<class State, class Data>
    using marked_state = marked_state<State, Data>;

  private:
    inner_transition inner_;

  public:
    marking_transition(inner_transition trans)
        : inner_(trans) {
    }

    template<class Data, class State, class OutIt>
    void operator()(marked_state<State, Data> const & source, OutIt out) {
      inner_(
          source.inner_state,
          boost::make_function_output_iterator([&](state_with_data<State, Data> sd) {
            *out++ = marked_state<State, Data>{
                  std::move(sd.key),
                  std::make_shared<path_mark<Data>>(source.parent_mark, sd.data)};
          }));
    }
  };
}} /* sokoban::search */

namespace std {
  template<class State, class Data>
  struct hash<typename sokoban::search::marked_state<State, Data>> {
    std::size_t operator()(sokoban::search::marked_state<State, Data> const& state) const {
      return std::hash<State>()(state.inner_state);
    }
  };
}

namespace sokoban { namespace search {
  template<class Transition>
  marking_transition<Transition> make_marking_transition(Transition trans) {
    return marking_transition<Transition>(trans);
  }

  template<class SourcesRange, class Transition, class DfsVisitor>
  void dfs(SourcesRange const & sources, Transition trans, DfsVisitor vis);

  template<class Transition, class DfsVisitor, class Node>
  struct dfs_engine {
    using visited_set = std::unordered_set<Node>;

  private:
    Transition trans_;
    DfsVisitor vis_;
    visited_set visited_;
    bool stopped_;

  public:
    dfs_engine(Transition trans, DfsVisitor vis)
        : trans_(trans)
        , vis_(vis)
        , stopped_(false) {
    }

    void run(Node const & src) {
      using namespace std::placeholders;

      if (stopped_)
        return;

      if (visited(src))
        return;

      if (vis_.open_vertex(src) == open_result::StopSearch)
      {
        stopped_ = true;
        return;
      }

      add_visited(src);

      trans_(src, boost::make_function_output_iterator(std::bind(&dfs_engine::run, this, _1)));
    }

  private:
    void add_visited(Node const & node) {
      visited_.insert(node);
    }

    bool visited(Node const & node) {
      return (visited_.find(node) != visited_.end());
    }
  };


  template<class SourcesRange, class Transition, class DfsVisitor>
  void dfs(SourcesRange const & sources, Transition trans, DfsVisitor vis) {
    using source_type =
      typename std::iterator_traits<
        typename boost::range_iterator<SourcesRange>::type>::value_type;

    dfs_engine<Transition, DfsVisitor, source_type> eng(trans, vis);
    for (auto s : sources) {
      eng.run(s);
    }
  }

}} /* sokoban::search */
