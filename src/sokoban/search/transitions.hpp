#pragma once

#include <boost/assign/list_of.hpp>

#include "sokoban/search/field_descriptor.hpp"
#include "sokoban/search/box_state.hpp"
#include "sokoban/search/direction.hpp"
#include "sokoban/search/dfs.hpp"
#include "deadlocks/simple.hpp"

namespace sokoban { namespace search {
  using sokoban::deadlocks::simple_deadlock_set;

  struct backtrack_data {
    std::ptrdiff_t  box_diff;
    direction last_push_direction;
    point old_box_pos;
    point new_box_pos;
    point old_player_point;
    int penalty;
  };

  template<class State, class OutIt, class PosIsValidF, class MoveBoxAndPlayerF>
  void push_transitions(
      field_descriptor const & fd,
      State const & source,
      OutIt out,
      PosIsValidF valid_pos,
      MoveBoxAndPlayerF mover) {

    reachable_boxes(source, boost::make_function_output_iterator(
      [&](box_and_direction bd) {
        auto push_dir = opposite_direction(bd.player_direction);
        auto box = *bd.box_it;

        auto fwd = adj_point_fwd(box, push_dir);

        if (valid_pos(fwd) && is_free(fd, source, fwd)) {
          *(out++) = state_with_data<State, backtrack_data>(
              mover(fd, source, bd.box_it, fwd, box),
              backtrack_data{
                std::distance(boost::begin(source.boxes()), bd.box_it),
                push_dir,
                box,
                fwd,
                source.positions().top_left(),
                0});
        }
      }));
  }

  template<class State, class OutIt, class PosIsValidF, class MoveBoxAndPlayerF>
  void pull_transitions(
      field_descriptor const & fd,
      State const & source,
      OutIt out,
      PosIsValidF valid_pos,
      MoveBoxAndPlayerF mover) {

    reachable_boxes(source, boost::make_function_output_iterator(
      [&](box_and_direction bd) {
        auto pull_dir = bd.player_direction;

        auto box = *bd.box_it;

        auto fwd  = adj_point_fwd(box, pull_dir);
        auto fwd2 = adj_point_fwd(fwd, pull_dir);

        if (valid_pos(fwd2) && is_free(fd, source, fwd2)) {
          *(out++) = state_with_data<State, backtrack_data>(
              mover(fd, source, bd.box_it, fwd, fwd2),
              backtrack_data{
                std::distance(boost::begin(source.boxes()), bd.box_it),
                pull_dir,
                box,
                fwd,
                source.positions().top_left(),
                0});
        }
    }));
  }

  namespace detail {
    template<class BoxesRange>
    box_state make_single_box_state(
          field_descriptor const & fd,
          point box,
          cached_positions positions,
          BoxesRange const & other_boxes) {
      std::vector<point> box_cont{box};
      return box_state(std::move(positions), std::move(box_cont));
    }

    template<class BoxesRange>
    box_state move_single_box_and_player(
          field_descriptor const & fd,
          box_state const & source,
          boost::range_iterator<box_state::boxes_const_range>::type box_it,
          point new_box,
          point new_player,
          BoxesRange const & other_boxes) {
      assert(boost::size(source.boxes()) == 1);
      assert(box_it == boost::begin(source.boxes()));

      std::vector<point> box_cont{new_box};
      cached_positions positions = compute_cached_positions(fd,
          boost::join(boost::assign::list_of(new_box), other_boxes), new_player);

      return box_state(std::move(positions), std::move(box_cont));
    }

    template<class BoxesRange>
    struct single_box_mover {
    private:
      BoxesRange const & other_boxes_;

    public:
      single_box_mover(BoxesRange const & other_boxes)
          : other_boxes_(other_boxes) {
      }

        box_state operator()(
            field_descriptor const & fd,
            box_state const & source,
            boost::range_iterator<box_state::boxes_const_range>::type box_it,
            point new_box,
            point new_player) {
        assert(boost::size(source.boxes()) == 1);
        assert(box_it == boost::begin(source.boxes()));

        std::vector<point> box_cont{new_box};
        cached_positions positions = compute_cached_positions(fd,
            boost::join(boost::assign::list_of(new_box), other_boxes_), new_player);

        return box_state(std::move(positions), std::move(box_cont));
      }
    };

    using single_push_data = hashable_data<box_state, backtrack_data>;

    template<bool Push, class BoxesRange>
    class move_single_box_transition {
      field_descriptor const & fd_;
      simple_deadlock_set const & dlocks_;
      BoxesRange const       & other_boxes_;

    public:
      move_single_box_transition(
              field_descriptor const & fd,
              simple_deadlock_set const & dlocks,
              BoxesRange const & other_boxes)
          : fd_(fd)
          , dlocks_(dlocks)
          , other_boxes_(other_boxes) {
      }
      template<class OutIt>
      void operator()(box_state const & st, OutIt out) {
        using namespace std::placeholders;

        auto valid_pos = [this](const point& p) {
          auto x = std::get<0>(p);
          auto y = std::get<1>(p);

          return x >= 0 && x < fd_.width() && y >= 0 && y < fd_.height()
            && !dlocks_.is_deadlocked(p)
            && boost::find(other_boxes_, p) == boost::end(other_boxes_);
        };
        auto out_trans = out;

        if (Push)
          return (push_transitions)(fd_, st, out_trans, valid_pos,
            single_box_mover<BoxesRange>(other_boxes_));
        else
          return (pull_transitions)(fd_, st, out_trans, valid_pos,
            single_box_mover<BoxesRange>(other_boxes_));
      }

      template<class OutIt>
      void operator()(single_push_data const & s, OutIt out) {
        using namespace std::placeholders;

        auto valid_pos = [this](const point& p) {
          auto x = std::get<0>(p);
          auto y = std::get<1>(p);

          return x >= 0 && x < fd_.width() && y >= 0 && y < fd_.height()
            && !dlocks_.is_deadlocked(p)
            && boost::find(other_boxes_, p) == boost::end(other_boxes_);
        };
        auto st = s.key;
        auto out_trans = out;

        if (Push)
          return (push_transitions)(fd_, st, out_trans, valid_pos,
            single_box_mover<BoxesRange>(other_boxes_));
        else
          return (pull_transitions)(fd_, st, out_trans, valid_pos,
            single_box_mover<BoxesRange>(other_boxes_));
      }
    };

    template<class Lambda>
    class func_visitor {
      Lambda lambda_;

    public:
      func_visitor(Lambda l)
          : lambda_(l){
      }

      template<class... Args>
      open_result open_vertex(Args ... args) {
        return lambda_(std::forward<Args>(args)...);
      }
    };

    template<class Lambda>
    func_visitor<Lambda> make_func_visitor(Lambda lam) {
      return func_visitor<Lambda>(lam);
    }
  }

  template<bool Push, class OutIt, class TargetPred>
  void direct_transitions_ex(
          field_descriptor const & fd,
          simple_deadlock_set const & dlocks,
          box_state const & source,
          boost::range_iterator<box_state::boxes_const_range>::type box_it,
          TargetPred tgt,
          OutIt out) {
    auto other_boxes = boost::copy_range<std::vector<point>>(
      source.boxes() | boost::adaptors::filtered([&](point box) {
          return box != *box_it;
        }));
    auto box_diff = std::distance(boost::begin(source.boxes()), box_it);
    auto visitor = detail::make_func_visitor(
        [&](detail::single_push_data const & st) {
          auto state = st.key;
          auto new_box_pos = *boost::begin(state.boxes());
          if (tgt(new_box_pos)) {
            std::vector<point> boxes(
              boost::begin(source.boxes()), boost::end(source.boxes()));
            *(boxes.begin() + box_diff) = new_box_pos;
            cached_positions positions = state.positions();

            *out++ = state_with_data<box_state, backtrack_data>(
                box_state(std::move(positions), std::move(boxes)),
                backtrack_data{
                  box_diff,
                  st.data.last_push_direction ,
                  *box_it,
                  new_box_pos,
                  source.positions().top_left()});
          }
          return open_result::ContinueSearch;
    });

    box_state search_source
      = detail::make_single_box_state(fd, *box_it, source.positions(), other_boxes);
    std::array<detail::single_push_data, 1> sources{{detail::single_push_data(
        search_source, backtrack_data())}};
    dfs(
      sources,
      detail::move_single_box_transition<Push, decltype(other_boxes)>(
          fd,
          dlocks,
          other_boxes),
      visitor);
  }

  template<bool Push, class OutIt>
  void direct_transitions(
          field_descriptor const & fd,
          simple_deadlock_set const & dlocks,
          box_state const & source,
          boost::range_iterator<box_state::boxes_const_range>::type box_it,
          OutIt out) {
    return direct_transitions_ex<Push>(fd, dlocks, source, box_it,
        [&](point new_box_pos) {
          return fd.goal_at(new_box_pos) && *box_it != new_box_pos;
        },
        out);
  }


  template<class OutIt>
  void simple_push_transitions(
      field_descriptor const & f,
      simple_deadlock_set const & dlocks,
      box_state const & source,
      OutIt out) {

    auto valid_pos = [&](const point& p) {
      auto x = std::get<0>(p);
      auto y = std::get<1>(p);

      return x >= 0 && x < f.width() && y >= 0 && y < f.height()
        && !dlocks.is_deadlocked(p);
    };

    for (auto box_it = boost::begin(source.boxes()), end_it = boost::end(source.boxes());
            box_it != end_it;
            ++box_it) {
      (direct_transitions<true>)(f, dlocks, source, box_it, out);
    }
    (push_transitions)(f, source, out, valid_pos, &move_box_and_player);
  }

  template<class OutIt>
  void simple_pull_transitions(
      field_descriptor const & f,
      box_state const & source,
      OutIt out) {

    auto valid_pos = [&](const point& p) {
      auto x = std::get<0>(p);
      auto y = std::get<1>(p);

      return x >= 0 && x < f.width() && y >= 0 && y < f.height();
    };

    (pull_transitions)(f, source, out, valid_pos, &move_box_and_player);
  }

  template<class OutIt>
  void simple_and_direct_pull_transitions(
      field_descriptor const & f,
      box_state const & source,
      OutIt out) {

    auto valid_pos = [&](const point& p) {
      auto x = std::get<0>(p);
      auto y = std::get<1>(p);

      return x >= 0 && x < f.width() && y >= 0 && y < f.height();
    };

    for (auto box_it = boost::begin(source.boxes()), end_it = boost::end(source.boxes());
            box_it != end_it;
            ++box_it) {
      (direct_transitions<false>)(f, deadlocks::create_empty_set(), source, box_it, out);
    }
    (pull_transitions)(f, source, out, valid_pos, &move_box_and_player);
  }



  /*template<class OutIt>
  void push_deadlock_transitions(
      field_descriptor const & f,
      box_state const & source,
      OutIt out) {

    namespace ba = boost::adaptors;

    auto valid_pos = [&](const point& p) {
      auto x = std::get<0>(p);
      auto y = std::get<1>(p);

      return -1 <= x && x <= f.width() && -1 <= y && y <= f.height();
    };

    auto move_box = [&](box_state<point> const & st,
        boost::range_iterator<box_state<point>::boxes_const_range>::type & box_it,
        point const & new_box_pos,
        point const & new_player_pos) -> box_state<point> {
      auto x = std::get<0>(new_box_pos);
      auto y = std::get<1>(new_box_pos);
      if (x < 0 || x >= f.width() || y < 0 || y >= f.height()) {
        return {
          new_player_pos,
          st.boxes() | ba::filtered([&](point const & box) { return *box_it != box; })
        };
      }

      return (move_box_and_player)(st, box_it, new_box_pos, new_player_pos);
    };

    (push_transitions)(f, source, out, valid_pos, move_box);
    (move_transitions)(f, source, out, valid_pos);
  } */
}} /* sokoban::search */
