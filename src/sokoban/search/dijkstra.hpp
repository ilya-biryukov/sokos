#pragma once

#include <boost/heap/binomial_heap.hpp>
#include <unordered_set>
#include <tuple>

#include "sokoban/search/path_mark.hpp"

namespace sokoban { namespace search {
  namespace detail {
    template<class Key, class Node>
    struct dijkstra_key {
      Key  key;
      Node node;
    };

    template<class Key, class Node>
    void swap(dijkstra_key<Key, Node>& lhs, dijkstra_key<Key, Node>& rhs) {
      swap(lhs.key, rhs.key);
      swap(lhs.node, rhs.node);
    }

    template<class Key, class Node>
    bool operator < (
          dijkstra_key<Key, Node> const & lhs,
          dijkstra_key<Key, Node> const & rhs) {
      // note: '>', since boost::heap implements max-heap
      return lhs.key > rhs.key;
    }
  }

  template<class Node>
  using edge_type = std::tuple<Node, int>;

  template<class InputRange, class DijkstraTransition, class DijkstraVisitor>
  void run_dijkstra(InputRange sources, DijkstraTransition trans, DijkstraVisitor vis);

  template<class Node, class DijkstraTransition, class DijkstraVisitor>
  struct dijkstra_engine {
    using node_type        = Node;
    using transititon_func = DijkstraTransition;
    using visitor_type     = DijkstraVisitor;
    using queue_key        = detail::dijkstra_key<int, node_type>;
    using edge_type        = search::edge_type<Node>;
    using queue            = boost::heap::binomial_heap<queue_key>;
    using visited_set      = std::unordered_set<Node>;

  private:
    queue open_;
    visited_set visited_;
    visitor_type visitor_;
    transititon_func transition_;
    bool stopped_;

  public:
    dijkstra_engine(transititon_func trans, visitor_type visitor)
        : visitor_(visitor)
        , transition_(trans)
        , stopped_(false) {
    }

    void add_vertex(node_type const& node) {
      add_vertex(node, 0);
    }

    void run() {
      while (!open_.empty()) {
        {
          queue_key const& top = open_.top();
          if (visit_vertex(top.node, top.key))
            return;
        }

        open_.pop();
      }
    }

  private:
    bool visit_vertex(node_type const & node, int dist) {
      using namespace std::placeholders;

      if (was_visited(node))
        return false;

      mark_visited(node);
      if (open_vertex(node, dist) == open_result::StopSearch)
        return true;

      transition_(node,
          boost::make_function_output_iterator(
            std::bind(&dijkstra_engine::handle_edge, this, node, dist, _1)));
      return stopped_;
    }

    void add_vertex(node_type const& node, int dist) {
      if (was_visited(node))
        return;

      open_.push(queue_key{dist, node});
    }

    void handle_edge(
          node_type const & src,
          int src_dist,
          edge_type const & tgt) {

      int tgt_dist = src_dist + std::get<1>(tgt);
      assert(tgt_dist >= src_dist && "negative edge not allowed");

      if (tgt_dist == src_dist) {
        if (visit_vertex(std::get<0>(tgt), tgt_dist)) {
          stopped_ = true;
        }
      }
      else {
        add_vertex(
            std::get<0>(tgt),
            tgt_dist);
      }
    }

    open_result open_vertex(node_type const & node, int dist) {
      if (stopped_)
        return open_result::StopSearch;
      return visitor_.open_vertex(node, dist);
    }

    void mark_visited(node_type const & n) {
      visited_.insert(n);
    }

    bool was_visited(node_type const & n) {
      return visited_.find(n) != visited_.end();
    }
  };

  template<class InputRange, class DijkstraTransition, class DijkstraVisitor>
  void dijkstra(InputRange sources, DijkstraTransition trans, DijkstraVisitor vis) {
    using source_type
      = typename std::iterator_traits<
          typename boost::range_iterator<InputRange>::type
        >::value_type;

    dijkstra_engine<source_type, DijkstraTransition, DijkstraVisitor> eng(trans, vis);
    for (auto s : sources) {
      eng.add_vertex(s);
    }

    eng.run();
  }
}} /* sokoban::search */
