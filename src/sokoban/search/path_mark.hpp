#pragma once

#include <memory>

namespace sokoban { namespace search {
  enum class open_result {
    ContinueSearch,
    StopSearch
  };

  template<class BackData>
  struct path_mark {
    using mark_data = BackData;

  private:
    std::shared_ptr<path_mark> parent_;
    mark_data data_;

  public:
    path_mark(std::shared_ptr<path_mark> parent, mark_data d)
        : parent_(parent)
        , data_(d) {
    }


    mark_data data() const {
      return data_;
    }

    std::shared_ptr<path_mark> parent() const {
      return parent_;
    }
  };
}} /* sokoban::search */
