#pragma once

#include <cassert>

namespace sokoban { namespace search {
  enum class direction {
    Up = 0,
    Down,
    Left,
    Right,
  };

  inline direction opposite_direction(direction dir) {
    switch (dir) {
      case direction::Up:
      case direction::Left:
        return (direction)((int)dir + 1);
      case direction::Down:
      case direction::Right:
        return (direction)((int)dir - 1);
      default:
        assert(false && "invalid direction");
    }
  }

  struct direction_iterator {
  private:
    direction value_;

  public:
    direction_iterator(direction value)
        : value_(value) {
    }
    inline direction_iterator& operator ++() {
      value_ = (direction)((int)value_ + 1);
      return *this;
    }

    inline direction operator *() {
      return value_;
    }
  };

  inline bool operator ==(direction_iterator lhs, direction_iterator rhs) {
    return *lhs== *rhs;
  }

  inline bool operator !=(direction_iterator lhs, direction_iterator rhs) {
    return !(lhs== rhs);
  }

  struct all_directions {};

  inline direction_iterator begin(all_directions) {
    return {direction::Up};
  }

  inline direction_iterator end(all_directions) {
    return {(direction)((int)direction::Right + 1)};
  }
}} /* sokoban::search */
