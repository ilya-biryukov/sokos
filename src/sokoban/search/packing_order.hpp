#pragma once

#include "sokoban/search/field_descriptor.hpp"
#include "sokoban/search/box_state.hpp"
#include "sokoban/search/astar.hpp"
#include "sokoban/search/transitions.hpp"
#include "deadlocks/simple.hpp"

namespace sokoban { namespace search {
  using point_collection = std::unordered_set<point>;

  namespace detail {
    struct pull_transition {
    public:
      using state = state_with_data<box_state, point_collection>;

    private:
      field_descriptor const & fd_;

    public:
      pull_transition(field_descriptor const & fd)
          : fd_(fd) {
      }

      template<class OutIt>
      void operator()(state const & st, OutIt out) {
        bool direct_move_found = false;
        auto const & s = st.key;
        auto const & old_collection = st.data;
        for (auto box_it = boost::begin(s.boxes()), end_it = boost::end(s.boxes());
              box_it != end_it;
              ++box_it) {
          bool push_for_box_found = false;
          direct_transitions<false>(
              fd_,
              deadlocks::create_empty_set(),
              s,
              box_it,
              boost::make_function_output_iterator(
                [&](state_with_data<box_state, backtrack_data> const & st) {
                  if (push_for_box_found)
                    return;
                  if (fd_.goal_at(st.data.new_box_pos)
                        && old_collection.find(st.data.new_box_pos) == old_collection.end()) {
                    push_for_box_found = true;

                    state_with_data<box_state, backtrack_data> new_st = std::move(st);
                    new_st.key.erase_box(boost::begin(new_st.key.boxes()) + st.data.box_diff);

                    backtrack_data penalized(st.data);
                    penalized.penalty += 1;
                    point_collection new_collection = old_collection;
                    new_collection.insert(st.data.new_box_pos);
                    *out++ = state_with_data<state, backtrack_data>{
                      state{std::move(new_st.key), std::move(new_collection)},
                      std::move(penalized)};
                  }
                }
              )
          );

          if (push_for_box_found) {
            direct_move_found = true;
            break;
          }
        }

        //std::cout << st.data.size() << std::endl;
        //std::cout << "Direct move: " << direct_move_found << std::endl;
        if (!direct_move_found) {
          simple_pull_transitions(
            fd_,
            s,
            boost::make_function_output_iterator(
              [&](state_with_data<box_state, backtrack_data> const & st) {
                  backtrack_data penalized(st.data);
                  penalized.penalty += 1;
                  *out++ = state_with_data<state, backtrack_data>{
                    state{st.key, old_collection},
                    std::move(penalized)};
              })
          );
        }
      }
    };

    inline field_descriptor reverse_field_descriptor(
          field_descriptor const & fd,
          box_state const & initial_state) {
      std::vector<point> goals;

      boost::copy(initial_state.boxes(), std::back_inserter(goals));
      return {fd.width(), fd.height(), fd.walls(), goals, fd.initial_player()};
    }
  }

  struct packing_step {
    point from;
    point to;
    bool  forced;
  };

  inline std::ostream & operator << (std::ostream & s, packing_step const & st) {
    return s << "STEP(" << st.from << ", " << st.to << ", " << st.forced << ")";
  }

  template<class OutIt>
  bool find_packing_sequence(
        field_descriptor const & fwd_fd,
        box_state const & initial_state,
        OutIt out) {
    using namespace detail;


    using state = marked_state<detail::pull_transition::state, backtrack_data>;

    auto fd = reverse_field_descriptor(fwd_fd, initial_state);

    auto weigh_fun
      = [&](int astar_score, state const & data) {
          return 1;
        };

    std::vector<point> boxes;
    boost::copy(fwd_fd.goals(), std::back_inserter(boxes));

    std::vector<state> sources;
    {
      std::unordered_set<point> visited;
      for (auto box : boxes) {
        for (auto dir : all_directions()) {
          auto fwd = adj_point_fwd(box, dir);
          if (fd.wall_at(fwd) || boost::find(boxes, fwd) != boost::end(boxes))
            continue;

          auto state = make_box_state(fd, boxes, fwd);
          if (visited.find(state.positions().top_left()) != visited.end())
            continue;
          visited.insert(state.positions().top_left());
          sources.push_back({{state, point_collection()}, nullptr});
        }
      }
    }

    std::shared_ptr<path_mark<backtrack_data>> res = nullptr;

    auto visitor = make_func_visitor([&](state const & st, int dist) {
        if (st.inner_state.data.size() == fd.goals_size()) {
          res = st.parent_mark;
          return open_result::StopSearch;
        }

        return open_result::ContinueSearch;
    });

    auto trivial_heuristics = [&](state const & st) {
      return 0;
    };

    sokoban::search::astar(
        sources,
        make_marking_transition(pull_transition(fd)),
        visitor,
        weigh_fun,
        trivial_heuristics);

    if (!res)
      return false;

    restore_path(
        res,
        [&](backtrack_data const & bd) {
          bool forced = !fd.goal_at(bd.new_box_pos);
          *out++ = packing_step{bd.new_box_pos, bd.old_box_pos, forced};
        });
    return true;
  }
}} /* sokoban::search */
