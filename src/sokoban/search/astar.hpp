#pragma once

#include <iterator>
#include <tuple>
#include <boost/range/iterator.hpp>
#include <boost/function_output_iterator.hpp>
#include "sokoban/search/dijkstra.hpp"
#include "sokoban/search/dfs.hpp"

namespace sokoban { namespace search {
  namespace detail {
    template<class Node, class AStarHeuristics, class AStarTransition, class WeighFun>
    struct astar_dijkstra_transition {
      using heuristics_func = AStarHeuristics;
      using transition_func = AStarTransition;
      using weigh_func      = WeighFun;
      using node            = Node;

    private:
      heuristics_func h_;
      transition_func tr_;
      weigh_func      w_;

    public:
      astar_dijkstra_transition(heuristics_func func, transition_func trans, weigh_func w)
            : h_(func)
            , tr_(trans)
            , w_(w) {
      }

      template<class OutIt>
      void operator()(node const & src, OutIt out) {
        auto src_h = h_(src);
        tr_(src, boost::make_function_output_iterator(
          [&](node const & tgt) {
              auto tgt_h = h_(tgt);
              *out++ = edge_type<node>{tgt, w_(src_h - tgt_h, tgt)};
        }));
      }
    };
  }

  template<
    class InputRange,
    class AStarVisitor,
    class AStarTransition,
    class WeighFun,
    class AStarHeuristics>
  void astar(
          InputRange const & sources,
          AStarTransition trans,
          AStarVisitor vis,
          WeighFun w,
          AStarHeuristics heur) {
    using source_type
      = typename std::iterator_traits<
          typename boost::range_iterator<InputRange>::type
        >::value_type;

    return dijkstra(
        sources,
        detail::astar_dijkstra_transition<
            source_type, AStarHeuristics, AStarTransition, WeighFun
          >(heur, trans, w),
        vis);
  }
}} /* sokoban::search */
