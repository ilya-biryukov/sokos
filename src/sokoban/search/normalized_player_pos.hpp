#pragma once

#include <functional>
#include <boost/operators.hpp>
#include "classic_search/position.hpp"

namespace sokoban { namespace search {
  struct normalized_player_pos
      : private boost::equality_comparable<normalized_player_pos> {
  private:
    point pos_;

  public:
    normalized_player_pos(point pt)
        : pos_(pt) {
    }

    point position() const {
      return pos_;
    }
  };

  bool operator == (normalized_player_pos const & lhs, normalized_player_pos const & rhs) {
    return lhs.position() == rhs.position();
  }
}} /* sokoban::search */

namespace classic_search {
  template<>
  bool is_reachable<sokoban::search::normalized_player_pos>(
          sokoban::search::normalized_player_pos source,
          point pt) {
  }
} /* classic_search */

namespace std {
  template<>
  struct hash<sokoban::search::normalized_player_pos> {
    std::size_t operator()(sokoban::search::normalized_player_pos const & pos) {
      return std::hash<classic_search::point>()(pos.position());
    }
  };
}
