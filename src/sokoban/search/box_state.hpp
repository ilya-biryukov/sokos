#pragma once

#include <set>
#include <vector>
#include <limits>
#include <boost/range/iterator_range.hpp>
#include <boost/operators.hpp>
#include <functional>
#include <boost/mpl/if.hpp>

#include "classic_search/position.hpp"
#include "sokoban/search/direction.hpp"
#include "sokoban/search/field_descriptor.hpp"
#include "sokoban/search/dfs.hpp"
#include "util/hash_util.h"

namespace sokoban { namespace search {
  using classic_search::point;

  namespace detail {
    template<bool DropDirection>
    class move_player_transition_base;

    template<>
    struct move_player_transition_base<true> {
      using point_in_result = point;

      static point_in_result make_point(point p, direction) {
        return p;
      }

      static point get_pos(point_in_result const & p) {
        return p;
      }
    };

    template<>
    struct move_player_transition_base<false> {
      using point_in_result = state_with_data<point, direction>;

      static point_in_result make_point(point p, direction dir) {
        return point_in_result(p, dir);
      }

      static point get_pos(point_in_result const & p) {
        return p.key;
      }
    };

    template<bool DropDirection, class BoxesRange>
    struct move_player_transition : private move_player_transition_base<DropDirection> {
    private:
      using Base = move_player_transition_base<DropDirection>;

      using typename Base::point_in_result;
      using Base::make_point;
      using Base::get_pos;

    private:
      field_descriptor const & fd_;
      BoxesRange const &       boxes_;

    public:
      move_player_transition(field_descriptor const & fd, BoxesRange const& boxes)
          : fd_(fd)
          , boxes_(boxes){
      }

      template<class OutIt>
      void operator()(point pos, OutIt out) {

        using point_bool = std::tuple<point_in_result, bool>;
        std::array<point_bool, 4> arr;
        {
         int ctr = 0;
         for (auto move_dir : all_directions()) {
           auto fwd = adj_point_fwd(pos, move_dir);
           arr[ctr++] = point_bool{make_point(fwd, move_dir), !fd_.wall_at(fwd)};
         }
        }

        for (auto bx : boxes_) {
          auto iter = boost::find_if(arr, [bx](point_bool ptb) {
            return get_pos(std::get<0>(ptb)) == bx;
          });
          if (iter != boost::end(arr))
            std::get<1>(*iter) = false;
        }

        for (auto dir : arr) {
          if (std::get<1>(dir))
            *(out++) = std::get<0>(dir);
        }
      }
    };

    template<bool DropDirection, class BoxesRange>
    move_player_transition<DropDirection, BoxesRange> make_move_player_transition(
          field_descriptor const & fd,
          BoxesRange const & rng) {
      return move_player_transition<DropDirection, BoxesRange>(fd, rng);
    }
  }

  class cached_positions {
    std::set<point> reachable_;
    point top_left_;

  public:
    cached_positions(int width, int height)
          : top_left_(
              {std::numeric_limits<int>::max(), std::numeric_limits<int>::max()}) {
    }

    bool is_reachable(point pt) const {
      return reachable_.find(pt) != reachable_.end();
    };

    point top_left() const {
      return top_left_;
    }

    void add_point(point pt) {
      if (pt < top_left_)
        top_left_ = pt;
      reachable_.insert(pt);
    }
  };

  template<class BoxesRange>
  inline cached_positions compute_cached_positions(
          field_descriptor const & fd,
          BoxesRange const & boxes,
          point pl) {
    cached_positions res(fd.width(), fd.height());

    class visitor {
      cached_positions& positions_;

    public:
      visitor(cached_positions & positions)
          : positions_(positions) {
      }

      open_result open_vertex(point const & pt) {
        this->positions_.add_point(pt);
        return open_result::ContinueSearch;
      }
    };

    std::array<point, 1> pts = {{pl}};
    dfs(pts, detail::make_move_player_transition<true>(fd, boxes), visitor(res));

    return res;
  }

  class box_state : boost::equality_comparable<box_state>  {
  public:
    using boxes_range       = boost::iterator_range<std::vector<point>::iterator>;
    using boxes_const_range = boost::iterator_range<std::vector<point>::const_iterator>;

  private:
    cached_positions positions_;
    std::vector<point> boxes_;

  public:
    box_state(cached_positions && cached_pos, std::vector<point> && boxes)
        : positions_(std::move(cached_pos))
        , boxes_(std::move(boxes)) {
    }

  public:
    boxes_const_range boxes() const {
      return {boxes_.begin(), boxes_.end()};
    }

    boxes_range boxes() {
      return {boxes_.begin(), boxes_.end()};
    }

    void erase_box(boost::range_iterator<boxes_range>::type it) {
      boxes_.erase(it);
    }

    cached_positions const & positions() const {
      return positions_;
    }

    template<class BoxesRange>
    friend box_state make_box_state(
        field_descriptor const & fd,
        BoxesRange const & boxes,
        point player);

    friend box_state move_box_and_player(
        field_descriptor const & fd,
        box_state const & st,
        boost::range_iterator<box_state::boxes_const_range>::type box_it,
        point box_pos,
        point player_pos);

    friend bool operator == (box_state const & lhs, box_state const & rhs) {
      if (lhs.positions().top_left() != rhs.positions().top_left())
        return false;

      std::unordered_set<point> lhs_set(lhs.boxes().begin(), lhs.boxes().end());
      std::unordered_set<point> rhs_set(rhs.boxes().begin(), rhs.boxes().end());
      return boost::equal(lhs_set, rhs_set);
    }
  };

}} /* sokoban::search */

namespace std {
  template<>
  struct hash<sokoban::search::box_state> {
    std::size_t operator()(sokoban::search::box_state const & st) const {
      std::size_t seed = 0;
      boost::hash_combine(seed, st.positions().top_left());
      boost::hash_range(seed, boost::begin(st.boxes()), boost::end(st.boxes()));

      return seed;
    }
  };
} /* std */

namespace sokoban { namespace search {

  template<class BoxesRange>
  inline box_state make_box_state(
        field_descriptor const & fd,
        BoxesRange const & boxes,
        point player) {
    cached_positions pos = compute_cached_positions(fd, boxes, player);
    std::vector<point> vec_boxes(boost::begin(boxes), boost::end(boxes));
    return {std::move(pos), std::move(vec_boxes)};
  }

  inline bool box_at(box_state const & st, point pt) {
    return boost::find(st.boxes(), pt) != boost::end(st.boxes());
  }

  struct box_and_direction {
    boost::range_iterator<box_state::boxes_const_range>::type box_it;
    direction player_direction;
  };


  template<class OutIt>
  inline void reachable_boxes(box_state const & st, OutIt out) {
    cached_positions const & positions = st.positions();
    for (auto box_it = boost::begin(st.boxes()), end_it = boost::end(st.boxes());
          box_it != end_it;
          ++box_it) {

      for (auto dir : all_directions()) {
        auto dir_pt = adj_point_fwd(*box_it, dir);
        if (positions.is_reachable(dir_pt))
          *(out++) = box_and_direction{box_it, dir};
      }
    }
  }

  inline box_state move_box_and_player(
        field_descriptor const & fd,
        box_state const & st,
        boost::range_iterator<box_state::boxes_const_range>::type box_it,
        point box_pos,
        point player_pos) {
    std::vector<point> boxes(boost::begin(st.boxes()), boost::end(st.boxes()));
    *(boxes.begin() + std::distance(boost::begin(st.boxes()), box_it)) = box_pos;

    cached_positions pos = compute_cached_positions(fd, boxes, player_pos);
    return {std::move(pos), std::move(boxes)};
  }

}} /* sokoban::search */

