#pragma once

#include <set>
#include <tuple>
#include <boost/range/iterator_range.hpp>

#include "classic_search/position.hpp"

namespace classic_search {
  struct field_descriptor {
    using wall_const_range
        = boost::iterator_range<std::set<point>::const_iterator>;
    using goal_const_range
        = boost::iterator_range<std::set<point>::const_iterator>;

    template<class WallInputRange, class GoalInputRange>
    field_descriptor(
            std::size_t width,
            std::size_t height,
            WallInputRange const & walls,
            GoalInputRange const & goals,
            point initial_player_pos)
        : width_(width)
        , height_(height)
        , walls_(boost::copy_range<std::set<point>>(walls))
        , goals_(boost::copy_range<std::set<point>>(goals))
        , player_(initial_player_pos)
    {
    }

    wall_const_range walls() const {
      return walls_;
    }

    goal_const_range goals() const {
      return goals_;
    }

    std::size_t goals_size() const {
      return goals_.size();
    }

    bool wall_at(point pt) const {
      return walls_.find(pt) != walls_.end();
    }

    bool goal_at(point pt) const {
      return goals_.find(pt) != goals_.end();
    }

    std::size_t width() const {
      return width_;
    }

    std::size_t height() const  {
      return height_;
    }

    point initial_player() const {
      return player_;
    }

  private:
    std::size_t width_;
    std::size_t height_;
    std::set<point> walls_;
    std::set<point> goals_;
    point player_;
  };

  inline bool valid_coordinate(field_descriptor const & fd, point pt) {
    return std::get<0>(pt) >= 0 && std::get<1>(pt) >= 0 &&
      (std::size_t)std::get<0>(pt) < fd.width() && (std::size_t)std::get<1>(pt) < fd.height();
  }

  inline bool can_place_at(field_descriptor const & fd, point pt) {
      return valid_coordinate(fd, pt)
        && !fd.wall_at(pt);
  }
} /* classic_search */
