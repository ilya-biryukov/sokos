#pragma once

#include <utility>
#include <vector>
#include <array>

namespace classic_search {

  typedef std::pair<int, int> point;

  template<class Pos>
  bool is_reachable(Pos source, point pt);

  template<>
  bool is_reachable<point>(point source, point pt);

  std::array<point, 4> adjacent_points(point pt);
} /* classic_search */
