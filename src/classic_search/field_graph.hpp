#pragma once

#include <memory>
#include <boost/graph/graph_traits.hpp>
#include <boost/shared_container_iterator.hpp>
#include <boost/make_shared.hpp>

#include "classic_search/field_descriptor.hpp"
#include "classic_search/state.hpp"

namespace classic_search {
  struct field_graph {
    typedef state<point>                      vertex_descriptor;
    typedef std::pair<
      vertex_descriptor, vertex_descriptor>   edge_descriptor;
    typedef boost::undirected_tag             directed_category;
    typedef boost::disallow_parallel_edge_tag edge_parallel_category;
    typedef boost::incidence_graph_tag        traversal_category;
    typedef boost::shared_container_iterator<
      std::vector<edge_descriptor>>           out_edge_iterator;
    typedef std::iterator_traits<
      out_edge_iterator>::difference_type     degree_size_type;

  public:

    field_graph(field_descriptor const & fld)
        : fld_(fld) {
    }

    field_descriptor const & field() const {
      return fld_;
    }

  private:
    field_descriptor const & fld_;
  };

  inline std::pair<field_graph::out_edge_iterator, field_graph::out_edge_iterator> out_edges(
      field_graph::vertex_descriptor s, field_graph const & g) {
    // NOTE: bgl requires this function to run in constant time,
    // but since we use only bfs this should not matter
    auto edges = boost::make_shared<std::vector<field_graph::edge_descriptor>>();
    {
      auto && fld = g.field();
      auto moves = move_transition(s, fld);
      auto pushes = pull_transitions(s, fld);

      for (auto && tgt : boost::join(moves, pushes)) {
        edges->push_back({s, tgt});
      }
    }


    return boost::make_shared_container_range(edges);
  }

  inline field_graph::degree_size_type out_degree(
      field_graph::vertex_descriptor s, field_graph const & g) {
    return boost::distance(out_edges(s, g));
  }
} /* classic_search */
