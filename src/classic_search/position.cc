#include "sokos_defs.h"

#include "position.hpp"

#include <tuple>

using namespace classic_search;

template<>
bool classic_search::is_reachable<point>(point source, point pt) {
  return source == pt;
}

std::array<point, 4> classic_search::adjacent_points(point pt) {
  std::array<point, 4> res = {{pt, pt, pt, pt}};
  ++std::get<0>(res[0]);
  --std::get<0>(res[1]);
  ++std::get<1>(res[2]);
  --std::get<1>(res[3]);

  return res;
}
