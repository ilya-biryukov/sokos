#pragma once

#include <vector>
#include <boost/range/iterator_range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/join.hpp>
#include <boost/algorithm/cxx11/all_of.hpp>

#include "classic_search/position.hpp"
#include "classic_search/field_descriptor.hpp"
#include "util/hash_util.h"

namespace classic_search {
  template<class PlayerPosition>
  struct state {
    typedef PlayerPosition
      player_position_type;
    typedef boost::iterator_range<std::vector<point>::const_iterator>
      boxes_const_range;
    typedef boost::iterator_range<std::vector<point>::iterator>
      boxes_range;

    state()
        : pos_(-1, -1) {
    }

    state(state const & other)
        : pos_(other.pos_)
        , boxes_(other.boxes_) {
      assert_invariant();
    }

    template<class InputRange>
    state(player_position_type player_pos, InputRange boxes)
        : pos_(player_pos)
        , boxes_(boost::copy_range<std::vector<point>>(boxes)) {
      assert_invariant();
    }

    player_position_type player() const {
      return pos_;
    }

    void set_player(player_position_type const & pos) {
      assert_invariant();
      pos_ = pos;
    }

    boxes_const_range boxes() const {
      return boost::make_iterator_range(boxes_.begin(), boxes_.end());
    }

    boxes_range boxes() {
      return boost::make_iterator_range(boxes_.begin(), boxes_.end());
    }

    friend bool operator ==(state const & lhs, state const & rhs) {
      return lhs.player() == rhs.player() && boost::equal(lhs.boxes(), rhs.boxes());
    }

    friend bool operator !=(state const & lhs, state const & rhs) {
      return !(lhs == rhs);
    }
  private:
    void assert_invariant() {
      assert(boost::find(boxes_, pos_) == boost::end(boxes_));
      assert(boost::algorithm::all_of(boxes_,
            [this](point pt) {
              auto first_find = boost::find(boxes_, pt);
              if (first_find == boxes_.end())
                return false;
              return std::find(++first_find, boxes_.end(), pt) == boxes_.end();
            }));
    }

  private:
    player_position_type pos_;
    std::vector<point>   boxes_;
  };

  template<class Pos>
  std::size_t hash_value(state<Pos> const & s) {
    std::size_t seed = 0;
    boost::hash_combine(seed, s.player());
    boost::hash_range(seed, boost::begin(s.boxes()), boost::end(s.boxes()));
    return seed;
  }

  inline std::vector<state<point>> move_transition(state<point> const & source
      , field_descriptor const & fld) {
    std::vector<state<point>> result;

    auto can_place = [&fld, &source](point pt) {
      auto&& bxs = source.boxes();
      return can_place_at(fld, pt)
        && boost::find(bxs, pt) == boost::end(bxs);
    };

    auto change_player_position = [&source](point new_pos) {
      return state<point>(new_pos, source.boxes());
    };

    auto positions = adjacent_points(source.player())
      | boost::adaptors::filtered(can_place);

    return boost::copy_range<std::vector<state<point>>>(
         positions | boost::adaptors::transformed(change_player_position)
    );
  }

  template<class Pos>
  std::vector<state<Pos>> push_box_transitions(state<Pos> const & source
      , point box
      , field_descriptor const & fld) {
    using namespace std::placeholders;

    namespace ba = boost::adaptors;

    auto add_pts = [](point lhs, point rhs) -> point {
      std::get<0>(lhs) += std::get<0>(rhs);
      std::get<1>(lhs) += std::get<1>(rhs);
      return lhs;
    };

    auto scalar_mult = [](int alpha, point pt) -> point {
      std::get<0>(pt) *= alpha;
      std::get<1>(pt) *= alpha;
      return pt;
    };

    auto can_place = [&fld, &source](point pt) -> bool {
      auto&& bxs = source.boxes();
      return std::get<0>(pt) >= 0 && std::get<1>(pt) >= 0
        && !fld.wall_at(pt)
        && (boost::find(bxs, pt) == boost::end(bxs));
    };

    auto can_place_at_diff = [&](point df) -> bool {
      return can_place(add_pts(box, df))
        && is_reachable(source.player(), add_pts(box, scalar_mult(-1, df)));
    };

    auto change_player_and_box_position = [&](point new_box) -> state<Pos> {
      return state<Pos>(box, source.boxes() | ba::replaced(box, new_box));
    };

    std::array<point, 4> diffs = {{{1,0},{-1,0},{0,1},{0,-1}}};
    return boost::copy_range<std::vector<state<Pos>>>(
        diffs
          | ba::filtered(can_place_at_diff)
          | ba::transformed(std::bind<point>(add_pts, box, _1))
          | ba::transformed(change_player_and_box_position)
    );
  }

  template<class Pos>
  std::vector<state<Pos>> push_transitions(state<Pos> const & source
      , field_descriptor const & fld) {
    std::vector<state<Pos>> pushed_states;

    for (auto&& box : source.boxes()) {
      boost::copy(
          push_box_transitions(source, box, fld),
          std::back_inserter(pushed_states));
    }

    return pushed_states;
  }

  template<class Pos>
  std::vector<state<Pos>> pull_box_transitions(state<Pos> const & source
      , point box
      , field_descriptor const & fld) {
    using namespace std::placeholders;

    namespace ba = boost::adaptors;

    auto add_pts = [](point lhs, point rhs) -> point {
      std::get<0>(lhs) += std::get<0>(rhs);
      std::get<1>(lhs) += std::get<1>(rhs);
      return lhs;
    };

    auto scalar_mult = [](int alpha, point pt) -> point {
      std::get<0>(pt) *= alpha;
      std::get<1>(pt) *= alpha;
      return pt;
    };

    auto can_place = [&fld, &source](point pt) -> bool {
      auto&& bxs = source.boxes();
      return std::get<0>(pt) >= 0 && std::get<1>(pt) >= 0
        && !fld.wall_at(pt)
        && (boost::find(bxs, pt) == boost::end(bxs));
    };

    auto can_place_at_diff = [&](point df) -> bool {
      return can_place(add_pts(box, scalar_mult(2, df)))
        && is_reachable(source.player(), add_pts(box, df));
    };

    auto change_player_and_box_position = [&](point new_player) -> state<Pos> {
      return state<Pos>(new_player, source.boxes() | ba::replaced(box, source.player()));
    };

    std::array<point, 4> diffs = {{{1,0},{-1,0},{0,1},{0,-1}}};
    return boost::copy_range<std::vector<state<Pos>>>(
        diffs
          | ba::filtered(can_place_at_diff)
          | ba::transformed(std::bind<point>(scalar_mult, 2, _1))
          | ba::transformed(std::bind<point>(add_pts, box, _1))
          | ba::transformed(change_player_and_box_position)
    );
  }

  template<class Pos>
  std::vector<state<Pos>> pull_transitions(state<Pos> const & source
      , field_descriptor const & fld) {
    std::vector<state<Pos>> pulled_states;

    for (auto&& box : source.boxes()) {
      boost::copy(
          pull_box_transitions(source, box, fld),
          std::back_inserter(pulled_states));
    }

    return pulled_states;
  }
} /* classic_search */

namespace std {
  template<class PlayerPos>
  struct hash<classic_search::state<PlayerPos>> {
    std::size_t operator()(classic_search::state<PlayerPos> const & s) const {
      return std::hash<PlayerPos>()(s.player())
        + util::hash_seq(begin(s.boxes()), end(s.boxes()));
    }
  };
} /* std */
