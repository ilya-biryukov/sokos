#pragma once

#include "sokoban/sokoban.h"

#include <vector>

namespace sokos {
  enum class move {
    Right,
    RightPush,
    Left,
    LeftPush,
    Down,
    DownPush,
    Up,
    UpPush
  };

  struct solver_result {
    bool is_reachable;
    std::vector<move> moves;

  };

  solver_result solve_field(sokoban::field f);
} /* sokos */
