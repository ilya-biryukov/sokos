#pragma once

#include "sokos_defs.h"

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <algorithm>

#include "sokoban/sokoban.h"
#include "classic_search/field_graph.hpp"

namespace deadlocks {
  namespace detail {
    typedef boost::unordered_map<typename boost::graph_traits<classic_search::field_graph>::vertex_descriptor,
      boost::default_color_type> color_map_type;

    void run_search(
        sokoban::field const & f,
        sokoban::field::coordinate_type start,
        boost::unordered_set<sokoban::field::coordinate_type> & res,
        color_map_type & colors);
  }

  inline sokoban::field leave_only_walls(sokoban::field const & fld_) {
    sokoban::field res = fld_;
    for (std::size_t x = 0; x < res.width(); ++x) {
      for (std::size_t y = 0; y < res.height(); ++y) {
        if (res(x, y) != sokoban::cell_state::Wall)
          res(x, y) = sokoban::cell_state::Empty;
      }
    }

    return std::move(res);
  }

  template<class OutIter>
  void collect_simple_deadlock_positions(sokoban::field const & fld, OutIter out) {
    using namespace sokoban;

    sokoban::field empty_fld = leave_only_walls(fld);

    boost::unordered_set<sokoban::field::coordinate_type> targets;
    boost::unordered_set<sokoban::field::coordinate_type> non_deadlocks;

    detail::color_map_type colors;

    for (std::size_t x = 0; x < fld.width(); ++x) {
      for (std::size_t y = 0; y < fld.height(); ++y) {
        if (sokoban::is_target(fld(x,y))) {
          targets.insert({x, y});
          detail::run_search(empty_fld, sokoban::field::coordinate_type(x, y)
              , non_deadlocks, colors);
        }
      }
    }

    for (size_t x = 0; x < fld.width(); ++x) {
      for (size_t y = 0; y < fld.height(); ++y) {
        if (fld({x,y}) != cell_state::Wall
            && !is_target(fld({x,y}))
            && non_deadlocks.find({x,y}) == non_deadlocks.end()) {
          *(out++) = {x,y};
        }
      }
    }
  }
}
