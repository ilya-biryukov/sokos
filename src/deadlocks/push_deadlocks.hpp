#pragma once

#include <unordered_set>
#include "sokoban/sokoban.h"

namespace deadlocks {
  typedef sokoban::field deadlock_field;

  struct deadlock_info {
  private:
    deadlock_field field_;

  public:
    inline deadlock_info(deadlock_field& fld)
        : field_(fld) {
    }

    inline deadlock_field const & field() const {
      return field_;
    }

  };
}

namespace std {
  template<>
  struct hash<deadlocks::deadlock_info> {
    std::size_t operator()(deadlocks::deadlock_info const & dinfo) const {
      return std::hash<sokoban::field>()(dinfo.field());
    }
  };
}

namespace deadlocks {
  struct deadlocks_set {
    typedef std::unordered_set<deadlock_info> container;

  private:
     container deadlocks_;

  public:
     deadlocks_set(container && deadlocks)
         : deadlocks_(std::move(deadlocks)) {
     }

     container::const_iterator cbegin() const {
       return deadlocks_.cbegin();
     }

     container::const_iterator cend() const {
       return deadlocks_.cend();
     }

     container::const_iterator begin() const {
       return deadlocks_.begin();
     }

     container::const_iterator end() const {
       return deadlocks_.end();
     }
  };

  deadlocks_set& compute_push_deadlocks(size_t width, size_t height);
} /* deadlocks */

