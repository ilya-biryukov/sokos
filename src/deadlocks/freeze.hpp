#pragma once

#include <unordered_map>
#include <boost/operators.hpp>

#include "sokoban/search/field_descriptor.hpp"
#include "deadlocks/simple.hpp"

namespace sokoban { namespace deadlocks {

  namespace detail {
    struct hash_key : private boost::equality_comparable<hash_key> {
      point      box;
      direction  dir;

      hash_key(point p, direction d)
          : box(p)
          , dir(d) {
      }

      friend bool operator == (hash_key const & lhs, hash_key const & rhs) {
        return lhs.box == rhs.box && lhs.dir == rhs.dir;
      }
    };
  }

}} /* sokoban::deadlocks */

namespace std {
  template<>
  struct hash<sokoban::deadlocks::detail::hash_key> {
    std::size_t operator()(sokoban::deadlocks::detail::hash_key const & k) const {
      using namespace sokoban::search;

      return 29 * std::hash<point>()(k.box) + std::hash<int>()((int)k.dir);
    }
  };
} /* std */

namespace sokoban { namespace deadlocks {

template<class BoxesRange>
bool is_frozen_deadlock(
    sokoban::search::field_descriptor const & fd,
    simple_deadlock_set const & simple_dlocks,
    BoxesRange const & boxes,
    typename boost::range_iterator<BoxesRange>::type box_it) {
  using namespace sokoban::search;

  auto switch_axis = [](direction dir) {
    if (dir == direction::Up || dir == direction::Down)
      return direction::Left;
    return direction::Up;
  };

  std::unordered_set<point> temp_walls;

  auto is_wall = [&](point pt) {
    if (fd.wall_at(pt))
      return true;
    return temp_walls.find(pt) == temp_walls.end();
  };

  auto box_at = [&](point pt) { return boost::find(boxes, pt) != boost::end(boxes); };

  bool frozen_on_non_goal = false;
  std::function<bool(point, direction)> is_blocked_along_axis
    = [&](point pt, direction dir) -> bool {
    auto fwd = adj_point_fwd(pt, dir);
    auto bwd = adj_point_bwd(pt, dir);
    // If there is a wall on the left
    //  or on the right side of the box then the box is blocked along this axis
    if (is_wall(fwd) || is_wall(bwd))
      return true;

    //If there is a simple deadlock square on both sides (left and right)
    // of the box the box is blocked along this axis
    if (simple_dlocks.is_deadlocked(fwd) && simple_dlocks.is_deadlocked(bwd))
      return true;

    // If there is a box on the left or right side then this box
    // is blocked if the other box is blocked.
    bool fwd_box = box_at(fwd);
    bool bwd_box = box_at(bwd);
    if (!fwd_box && !bwd_box)
      return false;

    bool result = false;
    {
      temp_walls.insert(pt);

      if (fwd_box && is_blocked_along_axis(fwd, switch_axis(dir)))
        result = true;
      if (result && !fd.goal_at(pt))
        frozen_on_non_goal = true;
      if (bwd_box && (!result || !frozen_on_non_goal) && is_blocked_along_axis(bwd, switch_axis(dir)))
        result = true;

      temp_walls.erase(temp_walls.find(pt));
    }

    if (result && !fd.goal_at(pt))
      frozen_on_non_goal = true;
    return result;
  };


  is_blocked_along_axis(*box_it, direction::Up);
  is_blocked_along_axis(*box_it, direction::Left);

  return frozen_on_non_goal;
}

}} /* sokoban::deadlocks */
