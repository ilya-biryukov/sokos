#include "sokos_defs.h"

#include <boost/range/adaptors.hpp>
#include "simple.hpp"
#include "sokoban/search/box_state.hpp"
#include "sokoban/search/transitions.hpp"
#include "sokoban/search/dfs.hpp"
#include "sokoban/search/direction.hpp"

using namespace sokoban::deadlocks;

namespace {
  struct transition {
  private:
    field_descriptor const & fd_;

  public:
    transition(field_descriptor const & fd)
        : fd_(fd) {
    }

    template<class OutIt>
    void operator()(box_state const & s, OutIt out) {
        return simple_pull_transitions(
            fd_,
            s,
            boost::make_function_output_iterator(
              [&](state_with_data<box_state, backtrack_data> const & st ) {
                *out++ = st.key;
              }
            )
        );
    }
  };
}

simple_deadlock_set sokoban::deadlocks::create_deadlock_set(field_descriptor const & fd) {
  namespace ba = boost::adaptors;

  std::unordered_set<point> non_deadlocks;

  class visitor {
    std::unordered_set<point>& non_dl_;

  public:
    visitor(std::unordered_set<point> & non_dl)
        : non_dl_(non_dl) {
    }

    open_result open_vertex(box_state const & st) {
      non_dl_.insert(*boost::begin(st.boxes()));
      return open_result::ContinueSearch;
    }
  };

  {
    std::vector<box_state> sources;
    for (auto goal : fd.goals()) {
      non_deadlocks.insert(goal);
      for (auto dir : all_directions()) {
        auto pt = adj_point_fwd(goal, dir);
        if (fd.wall_at(pt))
          continue;

        sources.push_back(make_box_state(fd, std::array<point, 1>{{goal}}, pt));
      }
    }

    dfs(sources, transition(fd), visitor(non_deadlocks));
  }

  std::unordered_set<point> deadlocks;
  for (int y = 0; y < fd.height(); ++y) {
    for (int x = 0; x < fd.width(); ++x) {
      if (non_deadlocks.find({x, y}) == non_deadlocks.end())
        deadlocks.insert({x, y});
    }
  }

  return {std::move(deadlocks)};
}
simple_deadlock_set sokoban::deadlocks::create_empty_set() {
   return {std::unordered_set<point>{}};
}
