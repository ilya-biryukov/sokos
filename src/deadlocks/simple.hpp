#pragma once

#include <unordered_set>

#include "sokoban/search/field_descriptor.hpp"

namespace sokoban { namespace deadlocks {
  using namespace sokoban::search;

  struct simple_deadlock_set {
  private:
    std::unordered_set<point> deadlocked_;

  private:
    simple_deadlock_set(std::unordered_set<point> && deadlocked)
          : deadlocked_(std::move(deadlocked)) {
    }

  public:
    bool is_deadlocked(point pt) const {
      return deadlocked_.find(pt) != deadlocked_.end();
    }

    friend simple_deadlock_set create_deadlock_set(field_descriptor const & fd);
    friend simple_deadlock_set create_empty_set();
  };

  simple_deadlock_set create_deadlock_set(field_descriptor const & fd);
  simple_deadlock_set create_empty_set();
}} /* sokoban::deadlocks */


