#include "sokos_defs.h"
#include "push_deadlocks.hpp"
#include "classic_search/field_graph.hpp"

using namespace classic_search;
using namespace deadlocks;

namespace {
  struct modified_state_graph : classic_search::field_graph {
    using field_graph::field_graph;
  };
}

deadlocks_set& deadlocks::compute_push_deadlocks(size_t width, size_t height) {
  assert(width >= 3 && height >= 3 && "width or height is less than 3");
  sokoban::field field(width, height);
}
