#include "sokos_defs.h"

#include <iostream>
#include <iterator>
#include <fstream>
#include <boost/unordered_set.hpp>

#include "solver.h"
#include "sokoban/sokoban.h"
#include "bdd/bdd_manager.h"

sokoban::cell_state state_by_char(char c) {
  using namespace sokoban;

  if (c == ' ')
    return cell_state::Empty;
  if (c == '#')
    return cell_state::Wall;
  if (c == '@')
    return cell_state::Player;
  if (c == '$')
    return cell_state::Box;
  if (c == '.')
    return cell_state::Target;
  if (c == '*')
    return cell_state::BoxTarget;
  if (c == '+')
    return cell_state::PlayerTarget;

  assert(false && "unknown character");
  return cell_state::Empty;
}

char char_by_state(sokoban::cell_state c) {
  using namespace sokoban;

  if (c == cell_state::Empty)
    return ' ';
  if (c == cell_state::Wall)
    return '#';
  if (c == cell_state::Player)
    return '@';
  if (c == cell_state::Box)
    return '$';
  if (c == cell_state::Target)
    return '.';
  if (c == cell_state::BoxTarget)
    return '*';
  if (c == cell_state::PlayerTarget)
    return '+';

  assert(false && "unknown cell_state");
  return '?';
}

int guess_node_num(sokoban::field const & f)
{
  int const kMaxSize = 300000000;
  int const kMinSize = 50000000;

  int const kMaxPeek = 250;
  int const kMinPeek = 50;
  int const kPeekDiff = kMaxPeek - kMinPeek;

  int const kAccel = (kMaxSize - kMinSize) / kPeekDiff / kPeekDiff;

/*  int fieldSize = f.width() * f.height();
  fieldSize = std::min(kMaxPeek, std::max(kMinPeek, fieldSize));
  fieldSize -= kMinPeek;

  return kMinSize + kAccel * fieldSize * fieldSize;*/
  return kMaxSize;
}

void print_usage() {
  std::cout << "Usage: sokos <sokoban-field>\n";
}

void print_deadlocks(
    sokoban::field const & f,
    boost::unordered_set<sokoban::field::coordinate_type> const & deadlocks) {
  using namespace sokoban;

  for (std::size_t y = 0; y < f.height(); ++y) {
    for (std::size_t x = 0; x < f.width(); ++x) {
      if (deadlocks.find({x, y}) == deadlocks.end())
        std::cout << char_by_state(f({x,y}));
      else
        std::cout << '~';
    }
    std::cout << '\n';
  }
}

int main(int argc, char * argv[]) {
  int const kCacheRatio = 4;
  int const kMaxIncrease = 5000000; // the docs say it's 100 Mb

  using namespace sokoban;

  if (argc != 2) {
    print_usage();
    return 0;
  }

  std::ifstream file(argv[1], std::ios::binary);
  if (!file) {
    std::cout << "Couldn't open file '" << argv[1] << "'\n";
    return 1;
  }

  std::noskipws(file);
  field f = read_field(std::istream_iterator<char>(file), std::istream_iterator<char>(),
      &state_by_char, [](char c) { return c == '\n'; });
  file.close();

  SOKOS_LOG_DEBUG("initializing BuDDY");

  auto node_num = guess_node_num(f);
  SOKOS_LOG_DEBUG("node num: " << node_num);

  bdd_util::buddy_package buddy(node_num);
  buddy.set_cache_ratio(kCacheRatio);
  buddy.set_max_increase(kMaxIncrease);

  SOKOS_LOG_DEBUG("Running solve_field");
  sokos::solver_result res = sokos::solve_field(f);
  if (!res.is_reachable)
    std::cout << "X";
  for (auto m : res.moves) {
    using namespace sokos;
    char c = '?';
    switch (m) {
      case move::Up:
      case move::UpPush:
        c = 'U';
        break;
      case move::Right:
      case move::RightPush:
        c = 'R';
        break;
      case move::Left:
      case move::LeftPush:
        c = 'L';
        break;
      case move::Down:
      case move::DownPush:
        c = 'D';
        break;
      default:
        assert(false && "unknown move member");
    }
    std::cout << c;
  }
  std::cout << std::endl;
}
