#include <bdd.h>
#include <fdd.h>
#include <vector>
#include <array>

#include "bdd/bdd_manager.h"

using namespace std;
using namespace bdd_util;

int main() {
  int const kVars = 2;
  int const kDomainSize = 4;

  vector<int> main(kVars);
  vector<int> other(kVars);
  vector<int> temp(kVars);

  for (int i = 0; i < kVars; ++i) {
    //main[i] = 3 * i;
    //temp[i] = 3 * i + 1;
    //other[i] = 3 * i + 2;
    main[i] = i;
    other[i] = kVars + i;
    temp[i] = 2 * kVars + i;
  }

  buddy_package mgr(100);

  array<int, 1> size{{kDomainSize}};
  for (int i = 0; i < 3 * kVars; ++i) {
    fdd_extdomain(size.data(), 1);
  }

  shared_ptr<bddPair> main_to_other = make_bdd_pair();
  fdd_setpairs(main_to_other.get(), main.data(), other.data(), kVars);
  shared_ptr<bddPair> other_to_main = make_bdd_pair();
  fdd_setpairs(other_to_main.get(), other.data(), main.data(), kVars);
  shared_ptr<bddPair> main_to_temp = make_bdd_pair();
  fdd_setpairs(main_to_temp.get(), main.data(), temp.data(), kVars);
  shared_ptr<bddPair> other_to_temp = make_bdd_pair();
  fdd_setpairs(other_to_temp.get(), other.data(), temp.data(), kVars);

  bdd main_vars = fdd_makeset(main.data(), kVars);
  bdd other_vars = fdd_makeset(other.data(), kVars);
  bdd temp_vars = fdd_makeset(temp.data(), kVars);


  auto rel = bddtrue;
  for (int i = 0; i < kVars; ++i) {
    auto rel_v = bddfalse;
    for (int v = 0; v < kDomainSize - 1; ++v) {
      rel_v &= fdd_ithvar(main[i], v) & fdd_ithvar(other[i], v + 1);
    }
    rel |= rel_v;
  }
  fdd_printset(rel);

  cout << endl;
  auto tmp1 = bdd_replace(rel, main_to_temp.get());
  fdd_printset(tmp1);
  cout << endl;
  auto tmp2 = bdd_replace(rel, other_to_temp.get());
  fdd_printset(tmp2);
  cout << endl;
  auto tmp3 = bdd_relprod(
      bdd_replace(rel, main_to_temp.get()),
      bdd_replace(rel, other_to_temp.get()), temp_vars);
  fdd_printset(tmp3);
  cout << endl;
}
